<?php
session_start();

if (!isset($_SESSION["user"])) {
    header('Location: ../login.php');
    exit;
}

require '../function.php';

//abil data di URL
$id = $_GET["id"];

//query data mahasiswa bedasarkan id 
$nusa = query("SELECT * FROM datanusa WHERE id = $id")[0];
//cek apakah tombol submit sudah di tekan atau belum
if (isset ($_POST["ubah"])){

//cek apakahdata berhasil di ubah atau belum
if (ubahnusa($_POST) > 0) {
    echo "
    <script>
    alert('Data Berhasil di Ubah!!!');
    document.location.href='../dashboard.php#nusasms';
    </script>
    ";
} else {
    echo "
    <script>
    alert('Data Gagal di Ubah!!!');
    document.location.href='../dashboard.php#nusasms';
    </script>
    ";
}
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png" href="../img/dawang.jpeg">
    <title>DLI | Ubah NusaSMS</title>
    <!-- Google Font: Source Sans Pro -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
        <link rel="stylesheet" href="../plugins/fontawesome-free/css/all.min.css">
    <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bootstrap 4 -->
        <link rel="stylesheet" href="../plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
    <!-- iCheck -->
        <link rel="stylesheet" href="../plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- JQVMap -->
        <link rel="stylesheet" href="../plugins/jqvmap/jqvmap.min.css">
    <!-- Theme style -->
        <link rel="stylesheet" href="../css/adminlte.min.css">
    <!-- overlayScrollbars -->
        <link rel="stylesheet" href="../plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
    <!-- Daterange picker -->
        <link rel="stylesheet" href="../plugins/daterangepicker/daterangepicker.css">
    <!-- summernote -->
        <link rel="stylesheet" href="../plugins/summernote/summernote-bs4.min.css">

</head>
<body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">
        
    <!-- navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="../dashboard.php" class="nav-link">Beranda</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="../keluar.php" class="nav-link">Keluar</a>
      </li>
    </ul>

    <!-- SEARCH FORM -->
    <!-- <form class="form-inline ml-3">
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fas fa-search"></i>
          </button>
        </div>
      </div>
    </form> -->

    <!-- Fullscreen -->
    <!-- <li class="nav-item">
        <a class="nav-link" data-widget="fullscreen" href="#" role="button">
        <i class="fas fa-expand-arrows-alt"></i></a>
    </li> -->
    </div>
    </nav>

    <aside class="main-sidebar sidebar-dark-success elevation-4">
        <!-- Logo -->
        <div class="user-panel mt-3 pb-3 d-flex">
            <div class="image">
                <img src="../img/dawang.jpeg" alt="PT Dawang Lestari Indah" class="brand-image img-circel elevation-2" style="opacity: .8">
            </div>
            <div class="info">
                <a style="color: white;">PT. Dawang Lestari Indah</a>
            </div>
        </div>
        <!-- Sidebar -->
        <div class="sidebar">
            <div class="user-panel mt-3 pb-3 d-flex">
                <div class="image">
                    <img src="../img/AdminLTELogo.png" class="img-circel elevation-2" alt="Dawang Lestasi Indah">
                </div>
                <div class="info">
                    <a class="d-block" style="color: white;"><?php echo $_SESSION['user'];?></a>
                </div>
            </div>

        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item menu-open">
                    <a href="#" class="nav-link active">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                            <p>Dawang Lestari Indah
                        <i class="right fas fa-angle-left"></i>
                            </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="../dashboard.php" class="nav-link">
                                <i class="fas fa-list nav-icon"></i>
                                <p>Monitoring</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="../dashboard2.php" class="nav-link">
                                <i class="fas fa-folder nav-icon"></i>
                                <p>Data File</p>
                            </a>
                        </li>
                        <!-- <li class=nav-item>
                            <a href="../dashboard3.php" class="nav-link">
                                <i class="fas fa-circel nav-icon"></i>
                                <p>Dashboard v3</p>
                            </a>
                        </li> -->
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
    </aside>

    <!-- content wrapper. contains page content -->
    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Ubah Data NusaSMS</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="../dashboard.php">Kembali</a></li>
                            <!-- <li class="breadcrumb-item"><a href="../datanusasms.php">NusaSMS</a></li> -->
                            <li class="breadcrumb-item active">Ubah data NusaSMS</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

            <section class="content">
                <div class="box-center">
                    <div class="card card-outline card-primary">
                        <div class="card-header text-center">
                            <h1>NusaSMS</h1>
                            <div class="card-body">
                                <form action="" method="POST" enctype="multipart/form-data"> 
                                    <input type="hidden" name="id" value="<?php echo $nusa["id"]; ?>">

                                    <div class="form-group">
                                        <label for="tanggal">Tanggal</label>
                                        <label><input type="date" size="20" name="tanggal" id="tanggal" class="form-control" required value="<?php echo $nusa['tanggal']?>"></label>
                                    </div>
                                        <!-- <table class="table center"> -->
                                    <div class="small-box bg-white">
                                        <!-- <tr> -->
                                            <div class="form-group">
                                                <label for="pagi">SMS :</label>
                                                <label><input type="text" class="form-control" size="6" name="smspagi" id="smspagi" onkeyup="sum();" autocomplete="off" placeholder="Angka Pagi" value="<?php echo $nusa['smspagi'];?>"></label>
                                                <label>-</label>
                                                <label><input type="text" class="form-control" size="6" name="smssore" id="smssore" onkeyup="sum();" autocomplete="off" placeholder="Angka Sore" value="<?php echo $nusa['smssore'];?>"></label>
                                                <label>=</label>
                                                <label><input type="text" class="form-control" size="6" name="smsjumlah" id="smsjumlah" autocomplete="off" placeholder="Jumlah SMS" value="<?php echo $nusa['smsjumlah'];?>"></label>
                                            </div> 
                                            <script>
                                                function sum() {
                                                    var txtFirstNumberValue = document.getElementById('smspagi').value;
                                                    var txtSecondNumberValue = document.getElementById('smssore').value;
                                                    var result = parseFloat(txtFirstNumberValue) - parseFloat(txtSecondNumberValue);
                                                    if (!isNaN(result)) {
                                                        document.getElementById('smsjumlah').value = result;
                                                        }
                                                    }
                                            </script>
                                        <!-- </tr> -->
                                        <!-- <tr>  -->
                                            <div class="form-group">
                                                <label for="pagi">WA :</label>
                                                <label><input type="text" class="form-control" size="6" name="wapagi" id="wapagi" onkeyup="sum2();" autocomplete="off" placeholder="Angka Pagi" value="<?php echo $nusa['wapagi'];?>"></label>
                                                <label>-</label>
                                                <label><input type="text" class="form-control" size="6" name="wasore" id="wasore" onkeyup="sum2();" autocomplete="off" placeholder="Angka Sore" value="<?php echo $nusa['wasore'];?>"></label>
                                                <label>=</label>
                                                <label><input type="text" class="form-control" size="6" name="wajumlah" id="wajumlah" autocomplete="off" placeholder="Jumlah WA" value="<?php echo $nusa['wajumlah'];?>"></label>
                                            </div>
                                            <script>
                                                function sum2() {
                                                    var txtFirstNumberValue2 = document.getElementById('wapagi').value;
                                                    var txtSecondNumberValue2 = document.getElementById('wasore').value;
                                                    var result = parseFloat(txtFirstNumberValue2) - parseFloat(txtSecondNumberValue2);
                                                    if (!isNaN(result)) {
                                                        document.getElementById('wajumlah').value = result;
                                                    }
                                                }
                                            </script>
                                        <!-- </tr> -->
                                        <!-- <tr> -->
                                            <div class="form-group">
                                                <label for="ket">Keterangan :</label> 
                                                <label><input type="text" class="form-control" name="ket" id="ket" autocomplete="off" placeholder="Keterangan" value="<?php echo $nusa['ket']?>"></label>
                                            </div>
                                        <!-- </tr> -->
                                        <!-- </table> -->
                                        <!-- <br> -->
                                        <button type="submit" name="ubah" id="ubah" class="btn btn-primary">Simpan</button>                
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>            
            </section>
        
    </div>

    <footer class="main-footer">
        <strong>Copyright &copy; 2020 <a href="#" style="color: green;">PT. Dawang Lestari Indah</a>.</strong>
        All rights Reserved.
        <div class="float-right d-none d-sm-inline-block">
            <b>Version</b> 0.0.1-pre
        </div>
    </footer>
    <aside class="control-sidebar control-sidebar-dark">

    </aside>
<!-- jQuery -->
<script src="../plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="../plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="../plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<script src="../plugins/sparklines/sparkline.js"></script>
<!-- JQVMap -->
<script src="../plugins/jqvmap/jquery.vmap.min.js"></script>
<script src="../plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
<!-- jQuery Knob Chart -->
<script src="../plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="../plugins/moment/moment.min.js"></script>
<script src="../plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="../plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="../plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="../plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="../js/adminlte.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../js/demo.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- <script src="js/pages/dashboard.js"></script> -->

</body>
</html>