<?php
session_start();

if (!isset($_SESSION["user"])) {
    header('Location: ../login.php');
    exit;
}

require '../function.php';

    $id  = $_GET['id'];
    $komodo = query("SELECT * FROM datauserkomodo WHERE id= $id")[0];

    if (isset($_POST["ubah"])) {
        
        if (ubahkomodo($_POST) > 0) {
            echo "<script>
                alert ('Data Berhasil di Ubah!');
                document.location.href='../dashboard.php#userkomodo';    
                </script>";
        } else {
            echo "<script>
                alert ('Data Gagal di Ubah!!!');
                document.location.href='../dashboard.php#userkomodo';
                </script>";
        }
    }

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png" href="../img/dawang.jpeg">
    <title>DLI | Ubah User Komodo</title>
    <!-- Google Font: Source Sans Pro -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
        <link rel="stylesheet" href="../plugins/fontawesome-free/css/all.min.css">
    <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bootstrap 4 -->
        <link rel="stylesheet" href="../plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
    <!-- iCheck -->
        <link rel="stylesheet" href="../plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- JQVMap -->
        <link rel="stylesheet" href="../plugins/jqvmap/jqvmap.min.css">
    <!-- Theme style -->
        <link rel="stylesheet" href="../css/adminlte.min.css">
    <!-- overlayScrollbars -->
        <link rel="stylesheet" href="../plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
    <!-- Daterange picker -->
        <link rel="stylesheet" href="../plugins/daterangepicker/daterangepicker.css">
    <!-- summernote -->
        <link rel="stylesheet" href="../plugins/summernote/summernote-bs4.min.css">

</head>
<body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">
        
    <!-- navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="../dashboard.php" class="nav-link">Beranda</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="../keluar.php" class="nav-link">Keluar</a>
      </li>
    </ul>

    <!-- SEARCH FORM -->
    <!-- <form class="form-inline ml-3">
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fas fa-search"></i>
          </button>
        </div>
      </div>
    </form> -->

    <!-- Fullscreen -->
    <!-- <li class="nav-item">
        <a class="nav-link" data-widget="fullscreen" href="#" role="button">
        <i class="fas fa-expand-arrows-alt"></i></a>
    </li> -->
    </div>
    </nav>

    <aside class="main-sidebar sidebar-dark-success elevation-4">
        <!-- Logo -->
        <div class="user-panel mt-3 pb-3 d-flex">
            <div class="image">
                <img src="../img/dawang.jpeg" alt="PT Dawang Lestari Indah" class="brand-image img-circel elevation-2" style="opacity: .8">
            </div>
            <div class="info">
                <a style="color: white;">PT. Dawang Lestari Indah</a>
            </div>
        </div>
        <!-- Sidebar -->
        <div class="sidebar">
            <div class="user-panel mt-3 pb-3 d-flex">
                <div class="image">
                    <img src="../img/AdminLTELogo.png" class="img-circel elevation-2" alt="Dawang Lestasi Indah">
                </div>
                <div class="info">
                    <a class="d-block" style="color: white;"><?php echo $_SESSION['user'];?></a>
                </div>
            </div>

        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item menu-open">
                    <a href="#" class="nav-link active">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                            <p>Dawang Lestari Indah
                        <i class="right fas fa-angle-left"></i>
                            </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="../dashboard.php" class="nav-link">
                                <i class="fas fa-list nav-icon"></i>
                                <p>Monitoring</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="../dashboard2.php" class="nav-link">
                                <i class="fas fa-folder nav-icon"></i>
                                <p>Data File</p>
                            </a>
                        </li>
                        <!-- <li class=nav-item>
                            <a href="../dashboard3.php" class="nav-link">
                                <i class="fas fa-circel nav-icon"></i>
                                <p>Dashboard v3</p>
                            </a>
                        </li> -->
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
    </aside>

    <!-- content wrapper. contains page content -->
    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Ubah User Komodo</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="../dashboard.php#userkomodo">Kembali</a></li>
                            <!-- <li class="breadcrumb-item"><a href="../datauserkomodo.php">User Komodo</a></li> -->
                            <li class="breadcrumb-item active">Ubah User Komodo</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

            <section class="content">
                <div class="box-center">
                    <div class="card card-outline card-primary">
                        <div class="card-header text-center">
                            <h1>User Komodo</h1>
                        </div>
                        <div class="card-body">
                            <form action="" method="POST" enctype="multipart/form-data">
                            <input type="hidden" name="id" value="<?php echo $komodo['id']; ?>">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-lg-6 col-6">
                                    <div class="text-center">
                                        <div class="small-box bg-white">
                                            <div class="form-group">
                                                <h5>Ubah Downline</h5>
                                            </div>
                                        </div>
                                    </div>
                                        <div class="form-group">
                                            <label for="tanggak">Tanggal</label>
                                            <input type="date" name="tanggal" id="tanggal" class="form-control" required value="<?php echo $komodo['tanggal'];?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="nama1">Nama Singkat : </label>
                                            <input type="text" class="form-control" name="nama1" id="nama1" autocomplete="off" required placeholder="Silahkan input nama singkat dari downline" value="<?php echo $komodo['nama1'];?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="nama2">Nama Lengkap :</label>
                                            <input type="text" class="form-control" name="nama2" id="nama2" autocomplete="off" required placeholder="Silahkan input nama lengkap dari downline" value="<?php echo $komodo['nama2'];?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="upline">Upline : </label>
                                            <input type="text" class="form-control" name="upline" id="upline" autocomplete="off" required placeholder="MASTER-MASTER" value="<?php echo $komodo['upline'];?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="Kelharga">Kelompok Harga :</label>
                                            <?php $kelharga = $komodo['kelharga'];?>
                                            <select name="kelharga" id="kelhagra" class="form-control">
                                                <option <?php echo ($kelharga == 'H2H - HARGA UNTUK MITRA H2H') ? "selected" : ""?>>H2H - HARGA UNTUK MITRA H2H</option>
                                                <option <?php echo ($kelharga == 'WEBNG - Harga Produk untuk Web NG')? "selected" : ""?>>WEBNG - Harga produk untuk Web NG</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="markup">Markup :</label>
                                            <input type="text" class="form-control" name="markup" id="markup" placeholder="0" value="<?php echo $komodo['markup'];?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="markup2">Markup Default ke Downline Baru :</label>
                                            <input type="text" class="form-control" name="markup2" id="markup2" placeholder="0" value="<?php echo $komodo['markup2'];?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-6">
                                        <div class="text-center">
                                            <div class="small-box bg-white">
                                                <div class="form-group">
                                                    <h5>Ubah Terminal</h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="namatem">Nama Terminal : </label>
                                            <input type="text" class="form-control" name="namatem" id="namatem" autocomplete="off" required placeholder="Silahkan input nama singkat dari downline" value="<?php echo $komodo['namatem'];?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="pemilik">Pemilik :</label>
                                            <input type="text" class="form-control" name="pemilik" id="pemilik" autocomplete="off" required placeholder="Silahkan input nama lengkap dari downline" value="<?php echo $komodo['pemilik'];?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="passtrx">Password Transaksi : </label>
                                            <input type="text" class="form-control" name="passtrx" id="passtrx" autocomplete="off" required placeholder="Password Transaksi" value="<?php echo $komodo['passtrx'];?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="passweb">Password WebUI : </label>
                                            <input type="text" class="form-control" name="passweb" id="passweb" autocomplete="off" required placeholder="Password WEBUI" value="<?php echo $komodo['passweb'];?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="ket2">Keterangan : </label>
                                            <input type="text" class="form-control" name="ket2" id="ket2" autocomplete="off" required placeholder="Keterangan" Value="<?php echo $komodo['ket2'];?>">
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" name="ubah" id="ubah" class="btn btn-primary btn-block">Simpan</button>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>            
            </section>
        
    </div>

    <footer class="main-footer">
        <strong>Copyright &copy; 2020 <a href="#" style="color: green;">PT. Dawang Lestari Indah</a>.</strong>
        All rights Reserved.
        <div class="float-right d-none d-sm-inline-block">
            <b>Version</b> 0.0.1-pre
        </div>
    </footer>
    <aside class="control-sidebar control-sidebar-dark">

    </aside>
<!-- jQuery -->
<script src="../plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="../plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="../plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<script src="../plugins/sparklines/sparkline.js"></script>
<!-- JQVMap -->
<script src="../plugins/jqvmap/jquery.vmap.min.js"></script>
<script src="../plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
<!-- jQuery Knob Chart -->
<script src="../plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="../plugins/moment/moment.min.js"></script>
<script src="../plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="../plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="../plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="../plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="../js/adminlte.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../js/demo.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- <script src="js/pages/dashboard.js"></script> -->

</body>
</html>