<?php
session_start();

if (!isset($_SESSION["user"])) {
    header('Location: ../../login.php');
    exit;
}

require '../../function.php';

if (isset($_POST["tambah"])) {

    if (tambahsmsmonitoring($_POST) > 0) {
        echo "<script>
                alert('Data Berhasil di Tambahkan!');
                document.location.href='tambahsmsmonitoring.php';
              </script>";
    }else{
        echo "<script>
                alert('Data Gagal di Tambahkan!!!');
                document.location.href='tambahsmsmonitoring.php';
              </script>";
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1.0"> -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png" href="../../img/dawang.jpeg">
    <title>DLI | Tambah SMSMonitoring</title>
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
        <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
    <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bootstrap 4 -->
        <link rel="stylesheet" href="../../plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
    <!-- iCheck -->
        <link rel="stylesheet" href="../../plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- JQVMap -->
        <link rel="stylesheet" href="../../plugins/jqvmap/jqvmap.min.css">
    <!-- Theme style -->
        <link rel="stylesheet" href="../../css/adminlte.min.css">
    <!-- overlayScrollbars -->
        <link rel="stylesheet" href="../../plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
    <!-- Daterange picker -->
        <link rel="stylesheet" href="../../plugins/daterangepicker/daterangepicker.css">
    <!-- summernote -->
        <link rel="stylesheet" href="../../plugins/summernote/summernote-bs4.min.css">
</head>
<body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">
        <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <ul class="navbar-nav">
            <li class="navbar-item">
                <a href="#" class="nav-link" data-widget="pushmenu" role="button"><i class="fas fa-bars"></i></a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="../../dashboard.php" class="nav-link">Beranda</a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="../../keluar.php" class="nav-link">Keluar</a>
            </li>
        </ul>
        </nav>
    </div>

    <aside class="main-sidebar sidebar-dark-success elevation-4">
    <!-- logo -->
    <div class="user-panel mt-3 pb-3 d-flex">
            <div class="image">
                <img src="../../img/dawang.jpeg" alt="PT Dawang Lestari Indah" class="brand-image img-circel elevation-2" style="opacity: .8">
            </div>
            <div class="info">
                <a style="color: white;">PT. Dawang Lestari Indah</a>
            </div>
        </div>
        <!-- Sidebar -->
        <div class="sidebar">
            <div class="user-panel mt-3 pb-3 d-flex">
                <div class="image">
                    <img src="../../img/AdminLTELogo.png" class="img-circel elevation-2" alt="Dawang Lestasi Indah">
                </div>
                <div class="info">
                    <a class="d-block" style="color: white;"><?php echo $_SESSION['user'];?></a>
                </div>
            </div>

        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item menu-open">
                    <a href="../../dashboard.php" class="nav-link active">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                            <p>Dawang Lestari Indah
                        <i class="right fas fa-angle-left"></i>
                            </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="../../dashboard.php" class="nav-link">
                                <i class="fas fa-list nav-icon"></i>
                                <p>Monitoring</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="../../dashboard2.php" class="nav-link">
                                <i class="fas fa-folder nav-icon"></i>
                                <p>Data File</p>
                            </a>
                        </li>
                        <!-- <li class=nav-item>
                            <a href="../../dashboard3.php" class="nav-link">
                                <i class="fas fa-circel nav-icon"></i>
                                <p>Dashboard v3</p>
                            </a>
                        </li> -->
                    </ul>
                </li>
            </ul>
        </nav>
        </div>
    </aside>

    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h4 class="m-0">Tambah SMS Monitoring</h4>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="../../dashboard.php#smsmonitoring">Kembali</a></li>
                            <!-- <li class="breadcrumb-item"><a href="../datasmsmonitoring.php">SMS Monitoring</a></li> -->
                            <li class="breadcrumb-item active">Tambah SMS Monitoring</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    
        <section class="content">
            <div class="box-center">
                <div class="card card-outline card-primary">
                    <div class="card-header text-center">
                        <h1>SMS Monitoring</h1>
                            <div class="container-fluid">
                                <!-- <div class="box-center"> -->
                                    <form action="" method="post">
                                    <div class="small-box bg-white">
                                        <div class="form-group">
                                            <label for="Tanggal">Tanggal</label>
                                            <label><input type="date" name="tanggal" id="tanggal" required class="form-control" size="30"></label>
                                        </div>
                                    </div>
                                <!-- </div> -->
                                <div class="row">
                                    <div class="col-lg-4 col-4">
                                        <div class="small-box bg-white">
                                            <label>Pagi</label>
                                        <!-- </div> -->
                                        <!-- <div class="card-body"> -->
                                            <div class="form-group">
                                                <!-- <div class="row"> -->
                                                    <!-- <label for="pagi">REMAINING QUOTA : </label> -->
                                                    <label><input type="text" class="form-control" name="remaining_quota_pagi" id="remaining_quota_pagi" onkeyup="sum2();" autocomplete="off" placeholder="REMAINING QUOTA" autofocus required></label>
                                                    <!-- <label for="pagi2">TOTAL USAGE : </label> -->
                                                    <label><input type="text" class="form-control" name="total_usage_pagi" id="total_usage_pagi" onkeyup="sum3();" autocomplete="off" placeholder="TOTAL USAGE" required></label>
                                                    <!-- <label for="pagi3">QUEUE MSQ : </label> -->
                                                    <label><input type="text" class="form-control" name="queue_msq_pagi" id="queue_msq_pagi" onkeyup="sum4();" autocomplete="off" placeholder="QUEUE MSQ" required></label>
                                                    <!-- <label for="pagi4">SUCCESS TRANS : </label> -->
                                                    <label><input type="text" class="form-control" name="success_trans_pagi" id="success_trans_pagi" onkeyup="sum5();" autocomplete="off" placeholder="SUCCESS TRANS" required></label>
                                                    <!-- <label for="pagi5">FAIL TRANS : </label> -->
                                                    <label><input type="text" class="form-control" name="fail_trans_pagi" id="fail_trans_pagi" onkeyup="sum6();" autocomplete="off" placeholder="FAIL TRANS" required></label>
                                                <!-- </div> -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-4">
                                        <div class="small-box bg-white">
                                            <label>Sore</label>
                                        <!-- </div> -->
                                        <!-- <div class="card-body"> -->
                                            <div class="form-group">
                                                <!-- <div class="row"> -->
                                                    <!-- <label for="sore">REMAINING QUOTA : </label> -->
                                                    <label><input type="text" class="form-control" name="remaining_quota_sore" id="remaining_quota_sore" onkeyup="sum2();" autocomplete="off" placeholder="REMAINING QUOTA" autofocus></label>
                                                    <!-- <label for="sore2">TOTAL USAGE : </label> -->
                                                    <label><input type="text" class="form-control" name="total_usage_sore" id="total_usage_sore" autocomplete="off" onkeyup="sum3();" placeholder="TOTAL USAGE"></label>
                                                    <!-- <label for="sore3">QUEUE MSQ : </label> -->
                                                    <label><input type="text" class="form-control" name="queue_msq_sore" id="queue_msq_sore" autocomplete="off" onkeyup="sum4();" placeholder="QUEUE MSQ"></label>
                                                    <!-- <label for="sore4">SUCCESS TRANS : </label> -->
                                                    <label><input type="text" class="form-control" name="success_trans_sore" id="success_trans_sore" autocomplete="off" onkeyup="sum5();" placeholder="SUCCESS TRANS"></label>
                                                    <!-- <label for="sore5">FAIL TRANS : </label> -->
                                                    <label><input type="text" class="form-control" name="fail_trans_sore" id="fail_trans_sore" autocomplete="off" onkeyup="sum6();" placeholder="FAIL TRANS"></label>
                                                <!-- </div> -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-4">
                                        <div class="small-box bg-white">
                                            <label>Jumlah</label>
                                        <!-- </div> -->
                                        <!-- <div class="card-body"> -->
                                            <div class="form-group">
                                                <!-- <div class="row"> -->
                                                    <!-- <label for="jumlah">REMAINING QUOTA :</label> -->
                                                    <label><input type="text" class="form-control" name="jumlah_remaining_quota" id="jumlah_remaining_quota" placeholder="0" autocomplete="off"></label>
                                                    <!-- <label for="jumlah">TOTL USAGE :</label> -->
                                                    <label><input type="text" class="form-control" name="jumlah_total_usage" id="jumlah_total_usage" placeholder="0" autocomplete="off"></label>
                                                    <!-- <label for="jumlah">QUEUE MSQ :</label> -->
                                                    <label><input type="text" class="form-control" name="jumlah_queue_msq" id="jumlah_queue_msq" placeholder="0" autocomplete="off"></label>
                                                    <!-- <label for="jumlah">SUCCESS TRANS :</label> -->
                                                    <label><input type="text" class="form-control" name="jumlah_success_trans" id="jumlah_success_trans" placeholder="0" autocomplete="off"></label>
                                                    <!-- <label for="jumlah">FAIL TRANS :</label> -->
                                                    <label><input type="text" class="form-control" name="jumlah_fail_trans" id="jumlah_fail_trans" placeholder="0" autocomplete="off"></label>
                                                <!-- </div> -->
                                            </div>
                                        </div>
                                    </div>
                                        <div class="col-lg-4 col-5">
                                            <div class="small-box bg-white">
                                                <label>Keterangan</label>
                                        <!-- </div> -->
                                        <!-- <div class="card-body"> -->
                                                    <div class="form-group">
                                                        <textarea name="keterangan" id="keterangan" cols="30" rows="7" placeholder="Keterangan" class="form-control" autocomplete="off"></textarea>
                                                    </div>
                                            </div>
                                                <button type="submit" name="tambah" id="tambah" class="btn btn-primary btn-block">Simpan</button>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </section>



    </div>
    <footer class="main-footer">
    <strong> Copyright &copy; 2020 <a href="#" style="color: green;">PT. Dawang Lestari Indah</a>.</strong>
    All rights Reserved.
    <div class="float-right d-none d-sm-inline-block">
        <b>Version</b> 0.0.1-pre
    </div>
    </footer>
    

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="../../plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="../../plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<script src="../../plugins/sparklines/sparkline.js"></script>
<!-- JQVMap -->
<script src="../../plugins/jqvmap/jquery.vmap.min.js"></script>
<script src="../../plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
<!-- jQuery Knob Chart -->
<script src="../../plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="../../plugins/moment/moment.min.js"></script>
<script src="../../plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="../../plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="../../plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="../../plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="../../js/adminlte.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../js/demo.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- <script src="js/pages/dashboard.js"></script> -->
<script src="../js/script.js"></script>

</body>
</html>