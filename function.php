<?php

$conn = mysqli_connect("localhost","root","","admin_lte");
// $conn   = mysqli_connect("sql100.epizy.com","epiz_27676844","jedm0bbKnca","epiz_27676844_adminlte");



function query($query){
    global $conn;
    $result = mysqli_query($conn, $query);
    $rows = [];
    while ($row = mysqli_fetch_assoc($result)) {
        $rows[] = $row;
    }
    return $rows;
}

function register($data){
    global $conn;

    $namalengkap = strtolower(stripslashes($data["nama_lengkap"]));
    $email       = mysqli_real_escape_string($conn, $data["email"]);
    $password    = mysqli_real_escape_string($conn, $data["password"]);
    $password2   = mysqli_real_escape_string($conn, $data["password2"]);

    //cek nama lengkap sudah ada atau belum
    $result = mysqli_query($conn, "SELECT nama_lengkap FROM user WHERE nama_lengkap='$namalengkap'");
    if (mysqli_fetch_assoc($result)) {
        echo "<script>
                alert('Nama Anda Sudah Terdaftar!');
            </script>";

            return false;
    }

    //konfimasi password
    if ($password !== $password2) {
        echo "<script>
                alert('Password Anda Tidak Sesuai!!');
            </script>";

            return false;
    }

    //enkripsi password
    $password = password_hash($password, PASSWORD_DEFAULT);

    //tambah user baru kedalam database
    mysqli_query($conn, "INSERT INTO user VALUE ('','$namalengkap','$email','$password')");

    return mysqli_affected_rows($conn);
}

function datapertanyaan($data){
    global $conn;
    
    $nama       = htmlspecialchars($data['nama']);
    $pertanyaan = htmlspecialchars($data['pertanyaan']);
    $jawab      = htmlspecialchars($data['jawab']);
    
    mysqli_query($conn, "INSERT INTO datapertanyaan VALUE ('','$nama','$pertanyaan','$jawab')");
    
    return mysqli_affected_rows($conn);
}

function hapuspertanyaan($id){
    global $conn;
    mysqli_query($conn, "DELETE FROM datapertanyaan WHERE id=$id");

    return mysqli_affected_rows($conn);
}

function datacatatan($data){
    global $conn;
    $judul   = htmlspecialchars($_POST["judul"]);
    $ket     = htmlspecialchars($_POST["ket"]);
    
    $namaFile   = $_FILES['dokumen']['name'];
    $ukuranFile = $_FILES['dokumen']['size'];
    $error      = $_FILES['dokumen']['error'];
    $tmpname    = $_FILES['dokumen']['tmp_name'];
    
    $file    = 'file/'.$namaFile;

    //cek apakah yang diupload adalah file
    $ekstensiDokumenValid   = [null, 'jpg','jpeg','png','doc','docx','pdf','xls','xlsx','zip','rar','sql','exe','txt','csv','eml','bak'];
    $ekstensiDokumen        = explode('.', $namaFile);
    $ekstensiDokumen        = strtolower(end($ekstensiDokumen));
    if (!in_array($ekstensiDokumen, $ekstensiDokumenValid)) {
        
        echo "<script>
              alert('Ekstensi yang anda Upload bukan File!!!');
               </script>";
               return false;
    }
    //cek jika file yang dimasukan ukurannya lebih besar
    if ($ukuranFile > 10000000) {
        echo "<script>
              alert('Ukuran File Anda $ukuranFile terlalu besar!!');
              </script>";
              return false;
    }
    $namaFileBaru   = basename($_FILES['dokumen']['name']);

    move_uploaded_file($tmpname, 'dokumen/'.$namaFileBaru);

    // return $namaFile;

    $query = "INSERT INTO datacatatan
            VALUE
            ('','$judul','$ket', '$namaFile', '$file')";

mysqli_query($conn, $query);

return mysqli_affected_rows($conn);
}

function hapuscatatan($noid){
    global $conn;

    $query   = mysqli_query($conn,"SELECT * FROM datacatatan WHERE noid=$noid");
    $dokumen = $query->fetch_array();

    $query_hapus = mysqli_query($conn,"DELETE FROM datacatatan WHERE noid=$noid");
        if (is_file("dokumen/".$dokumen['dokumen'])) {
            unlink("dokumen/".$dokumen['dokumen']);
        }

    return mysqli_affected_rows($conn);

}

// function mencari($keyword){
//         global $conn;
//         $query = "SELECT * FROM datanusa
//         tanggal   like '$keyword' OR
//         smspagi   like '$keyword' OR
//         smssore   like '$keyword' OR
//         wapagi    like '$keyword' OR
//         wasore    like '$keyword' OR
//         smsjumlah like '$keyword' OR
//         wajumlah  like '$keyword' OR
//         ket       like '$keyword'
//         ";
//         return query($query);
// }

function tambahdatanusa($data){
    global $conn;
    $tanggal    = htmlspecialchars($data["tanggal"]);
    $smspagi    = htmlspecialchars($data["smspagi"]);
    $smssore    = htmlspecialchars($data["smssore"]);
    $wapagi     = htmlspecialchars($data["wapagi"]);
    $wasore     = htmlspecialchars($data["wasore"]);
    $smsjumlah  = htmlspecialchars($data["smsjumlah"]);
    $wajumlah   = htmlspecialchars($data["wajumlah"]);
    $ket        = htmlspecialchars($data["ket"]);

    $query = "INSERT INTO datanusa
                VALUE 
                ('','$tanggal','$smspagi','$smssore','$wapagi','$wasore','$smsjumlah','$wajumlah','$ket')";
            
    mysqli_query($conn, $query);

    return mysqli_affected_rows($conn);
    // }
}

function hapus3($id){
    global $conn;
    mysqli_query($conn, "DELETE FROM datanusa WHERE id = $id");

    return mysqli_affected_rows($conn);
}

function ubahnusa($data){
    global $conn;
    $id         = ($data["id"]);
    $tanggal    = htmlspecialchars($data["tanggal"]);
    $smspagi    = htmlspecialchars($data["smspagi"]);
    $smssore    = htmlspecialchars($data["smssore"]);
    $wapagi     = htmlspecialchars($data["wapagi"]);
    $wasore     = htmlspecialchars($data["wasore"]);
    $smsjumlah  = htmlspecialchars($data["smsjumlah"]);
    $wajumlah   = htmlspecialchars($data["wajumlah"]);
    $ket        = htmlspecialchars($data["ket"]);

    $query = "UPDATE datanusa SET
                tanggal   = '$tanggal',
                smspagi   = '$smspagi',
                smssore   = '$smssore',
                wapagi    = '$wapagi',
                wasore    = '$wasore',
                smsjumlah = '$smsjumlah',
                wajumlah  = '$wajumlah',
                ket       = '$ket'
                WHERE id  = $id";
    
    mysqli_query($conn, $query);

    return mysqli_affected_rows($conn);

}


function tambahsmsmonitoring($data){
    global $conn;
    $tanggal2        = htmlspecialchars($data["tanggal"]);
    $reminingquota   = htmlspecialchars($data["remaining_quota_pagi"]);
    $totalusage      = htmlspecialchars($data["total_usage_pagi"]);
    $queuemsq        = htmlspecialchars($data["queue_msq_pagi"]);
    $successtrans    = htmlspecialchars($data["success_trans_pagi"]);
    $failtrans       = htmlspecialchars($data["fail_trans_pagi"]);

    $reminingquota2  = htmlspecialchars($data["remaining_quota_sore"]);
    $totalusage2     = htmlspecialchars($data["total_usage_sore"]);
    $queuemsq2       = htmlspecialchars($data["queue_msq_sore"]);
    $successtrans2   = htmlspecialchars($data["success_trans_sore"]);
    $failtrans2      = htmlspecialchars($data["fail_trans_sore"]);

    $jumlah          = htmlspecialchars($data["jumlah_remaining_quota"]);
    $jumlah2         = htmlspecialchars($data["jumlah_total_usage"]);
    $jumlah3         = htmlspecialchars($data["jumlah_queue_msq"]);
    $jumlah4         = htmlspecialchars($data["jumlah_success_trans"]);
    $jumlah5         = htmlspecialchars($data["jumlah_fail_trans"]);

    $ket             = htmlspecialchars($data["keterangan"]);

    $query = "INSERT INTO datasmsmonitoring
                VALUE ('','$tanggal2','$reminingquota','$totalusage','$queuemsq','$successtrans','$failtrans','$reminingquota2','$totalusage2','$queuemsq2','$successtrans2','$failtrans2','$jumlah','$jumlah2','$jumlah3','$jumlah4','$jumlah5','$ket')";

    mysqli_query($conn, $query);

    return mysqli_affected_rows($conn);
}

function ubahsms($data){
    global $conn;
    $id              = ($data["id"]);
    $tanggal2        = htmlspecialchars($data["tanggal"]);
    $reminingquota   = htmlspecialchars($data["remaining_quota_pagi"]);
    $totalusage      = htmlspecialchars($data["total_usage_pagi"]);
    $queuemsq        = htmlspecialchars($data["queue_msq_pagi"]);
    $successtrans    = htmlspecialchars($data["success_trans_pagi"]);
    $failtrans       = htmlspecialchars($data["fail_trans_pagi"]);

    $reminingquota2  = htmlspecialchars($data["remaining_quota_sore"]);
    $totalusage2     = htmlspecialchars($data["total_usage_sore"]);
    $queuemsq2       = htmlspecialchars($data["queue_msq_sore"]);
    $successtrans2   = htmlspecialchars($data["success_trans_sore"]);
    $failtrans2      = htmlspecialchars($data["fail_trans_sore"]);

    $jumlah          = htmlspecialchars($data["jumlah_remaining_quota"]);
    $jumlah2         = htmlspecialchars($data["jumlah_total_usage"]);
    $jumlah3         = htmlspecialchars($data["jumlah_queue_msq"]);
    $jumlah4         = htmlspecialchars($data["jumlah_success_trans"]);
    $jumlah5         = htmlspecialchars($data["jumlah_fail_trans"]);

    $keterangan      = htmlspecialchars($data["keterangan"]);

    $query = "UPDATE datasmsmonitoring SET
                tanggal                 = '$tanggal2',
                remaining_quota_pagi    = '$reminingquota',
                total_usage_pagi        = '$totalusage',
                queue_msq_pagi          = '$queuemsq',
                success_trans_pagi      = '$successtrans',
                fail_trans_pagi         = '$failtrans',
                
                remaining_quota_sore    = '$reminingquota2',
                total_usage_sore        = '$totalusage2',
                queue_msq_sore          = '$queuemsq2',
                success_trans_sore      = '$successtrans2',
                fail_trans_sore         = '$failtrans2',
                
                jumlah_remaining_quota  = '$jumlah',
                jumlah_total_usage      = '$jumlah2',
                jumlah_queue_msq        = '$jumlah3',
                jumlah_success_trans    = '$jumlah4',
                jumlah_fail_trans       = '$jumlah5',
                
                keterangan              = '$keterangan'
                WHERE id = $id";

    mysqli_query($conn, $query);

    return mysqli_affected_rows($conn);

}

function hapus2($id){
    global $conn;
    mysqli_query($conn, "DELETE FROM datasmsmonitoring WHERE id=$id");

    return mysqli_affected_rows($conn);
}

function tambahdatauserkomodo($data){
    global $conn;
    $tanggal        = htmlspecialchars($data["tanggal"]);

    $nama_singkat   = htmlspecialchars($data["nama1"]);
    $nama_lengkap   = htmlspecialchars($data["nama2"]);
    $upline         = htmlspecialchars($data["upline"]);
    $kel            = htmlspecialchars($data["kelharga"]);
    $markup         = htmlspecialchars($data["markup"]);
    $markup2        = htmlspecialchars($data["markup2"]);

    $nama_terminal  = htmlspecialchars($data["namatem"]);
    $pemilik        = htmlspecialchars($data["pemilik"]);
    $pass           = htmlspecialchars($data["passtrx"]);
    $pass2          = htmlspecialchars($data["passweb"]);

    $ket2            = htmlspecialchars($data["ket2"]);

    $query = "INSERT INTO datauserkomodo
                VALUE
                ('','$tanggal','$nama_singkat','$nama_lengkap','$upline','$kel','$markup','$markup2','$nama_terminal','$pemilik','$pass','$pass2','$ket2')";
    
    mysqli_query($conn, $query);

    return mysqli_affected_rows($conn);
}

function ubahkomodo($data){
    global $conn;
    $id             = ($data['id']);
    $tanggal        = htmlspecialchars($data["tanggal"]);

    $nama_singkat   = htmlspecialchars($data["nama1"]);
    $nama_lengkap   = htmlspecialchars($data["nama2"]);
    $upline         = htmlspecialchars($data["upline"]);
    $kel            = htmlspecialchars($data["kelharga"]);
    $markup         = htmlspecialchars($data["markup"]);
    $markup2        = htmlspecialchars($data["markup2"]);

    $nama_terminal  = htmlspecialchars($data["namatem"]);
    $pemilik        = htmlspecialchars($data["pemilik"]);
    $pass           = htmlspecialchars($data["passtrx"]);
    $pass2          = htmlspecialchars($data["passweb"]);

    $ket2           = htmlspecialchars($data["ket2"]);

    $query = "UPDATE datauserkomodo SET
                    tanggal   = '$tanggal',
                    
                    nama1     = '$nama_singkat',
                    nama2     = '$nama_lengkap',
                    upline    = '$upline',
                    kelharga  = '$kel',
                    markup    = '$markup',
                    markup2   = '$markup2',
                    
                    namatem   = '$nama_terminal',
                    pemilik   = '$pemilik',
                    passtrx   = '$pass',
                    passweb   = '$pass2',
                    
                    ket2       = '$ket2'
                    WHERE  id = $id";

        mysqli_query($conn, $query);

        return mysqli_affected_rows($conn);
}

function hapus($id){
    global $conn;
    mysqli_query($conn, "DELETE FROM datauserkomodo WHERE id = $id ");

    return mysqli_affected_rows($conn);
}

?>
