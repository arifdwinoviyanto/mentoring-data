<?php
    include 'function.php';

    //plugin PHPExcel
    require_once 'PHPExcel/PHPExcel.php';

    //panggil class PHPExcel nya
    $excel = new PHPExcel();

    //setting awal file excel
    $excel->getproperties()->setCreator('My Notes Code')
                           ->setLastModifiedBy('My Notes Code')
                           ->setTitle("Data User Komodo")
                           ->setSubject("Transaksi")
                           ->setDescription("Laporan Semua Data User Komodo")
                           ->setKeywords("Data User Komodo");

    //membuat variable untuk menampung pengaturan style dari header table
    $style_col = array(
        'font' => array('bold' => true),//set font tebal

        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
        ),// set text berada di tengah secara vertical
        'borders' => array(
            'top'    => array('style' => PHPExcel_Style_Border::BORDER_THIN),
            'right'  => array('style' => PHPExcel_Style_Border::BORDER_THIN),
            'bootom' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
            'left'   => array('style' => PHPExcel_Style_Border::BORDER_THIN)
        )
    );

    //membuat variable penampung style dari isi table
    $style_row = array(
        'alignment' => array(
            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
        ),
        'borders' => array(
            'top'    => array('style' => PHPExcel_Style_Border::BORDER_THIN),
            'right'  => array('style' => PHPExcel_Style_Border::BORDER_THIN),
            'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
            'left'   => array('style' => PHPExcel_Style_Border::BORDER_THIN)
        )
    );


    //eksekusi 
    if (isset($_GET['tgl_awal3']) && isset($_GET['tgl_akhir3'])) {
        $tgl = date('Y-m-d', strtotime($_GET['tgl_awal3']));
        $tgl2 = date('Y-m-d', strtotime($_GET['tgl_akhir3']));

        $label = 'Data User Komodo Tanggal '.$tgl.' sd '.$tgl2;

        $query = "SELECT * FROM datauserkomodo WHERE DATE(tanggal)>='".$tgl."' AND DATE(tanggal)<='".$tgl2."' ORDER BY tanggal ASC";
    } else {
        $label = 'Semua Data User Komodo';

        $query = "SELECT * FROM datauserkomodo ORDER BY tanggal ASC";
    }


    $excel->setActiveSheetIndex(0);
    $excel->getActiveSheet()->setCellValue('A1', 'Data User Komodo');//set Kolom A
    $excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(16);
    $excel->getActiveSheet()->getStyle('A1')->applyFromArray(
        array(
            'alignment' => array(
                'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'font'       => array('size' => 12)
            )
        )
    );
    $excel->getActiveSheet()->mergeCells('A1:D1');//set panjang margin baris 1
    $excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);//set font bold

    $excel->getActiveSheet()->setCellValue('A2', $label);
    $excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(12);
    $excel->getActiveSheet()->mergeCells('A2:D2');
    $excel->getActiveSheet()->getStyle('A2')->applyFromArray(
        array(
            'alignment' => array(
                'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'font'       => array('size' => 12)
            )
        )
    );

    //membuat header table pada baris ke 4
    $excel->getActiveSheet()->setCellValue('A4', 'Tanggal');
    $excel->getActiveSheet()->setCellValue('B4', 'Nama Downline');
    $excel->getActiveSheet()->setCellValue('C4', 'Nama Terminal');
    $excel->getActiveSheet()->setCellValue('D4', 'Keterangan');
    $excel->getActiveSheet()->getStyle('A4:D4')->getFont()->setSize(12);
    
    //apply style header yang telah di buat ke masing-masing kolom header
    $excel->getActiveSheet()->getStyle('A4')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('B4')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('C4')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('D4')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('A4:D4')->getFill()->applyFromArray(
        array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'startcolor' => array('argb' => 'FF008000')
        )
    );
    
    //set Height bari ke 1 sampai 4
    $excel->getActiveSheet()->getRowDimension('1')->setRowHeight(20);
    $excel->getActiveSheet()->getRowDimension('2')->setRowHeight(20);
    $excel->getActiveSheet()->getRowDimension('3')->setRowHeight(20);
    $excel->getActiveSheet()->getRowDimension('4')->setRowHeight(20);

    $sql = mysqli_query($conn, $query); //eksekusi jalankan query
    $no = 1; //untk penomoran tabel, di awal set dengan 1
    $numrow = 5; //baris pertama untuk di isi tabel adalah baris ke 5
    $numrow2 = 6; //baris kedua untuk di isi tabel adalah baris ke 6
    $numrow3 = 7; //baris ketiga untuk di isi tabel adalah bari ke 7
    $numrow4 = 8; //baris keempat untuk di isi tabel adalah baris ke 8
    $numrow5 = 9; //baris kelima untuk di isi tabel adalah baris ke 9
    $numrow6 = 10; //baris ke enam untuk di isi tabel adalah baris ke 10

    while ($data = mysqli_fetch_array($sql)) {
        $tgl = date('Y-m-d', strtotime($data['tanggal']));

        $excel->getActiveSheet()->setCellValue('A'.$numrow, $tgl);
        $excel->getActiveSheet()->setCellValue('B'.$numrow, $data['nama1']);
        $excel->getActiveSheet()->setCellValue('B'.$numrow2, $data['nama2']);
        $excel->getActiveSheet()->setCellValue('B'.$numrow3, $data['upline']);
        $excel->getActiveSheet()->setCellValue('B'.$numrow4, $data['kelharga']);
        $excel->getActiveSheet()->setCellValue('B'.$numrow5, $data['markup']);
        $excel->getActiveSheet()->setCellValue('B'.$numrow6, $data['markup2']);
        $excel->getActiveSheet()->setCellValue('C'.$numrow, $data['namatem']);
        $excel->getActiveSheet()->setCellValue('C'.$numrow2, $data['pemilik']);
        $excel->getActiveSheet()->setCellValue('C'.$numrow3, $data['passtrx']);
        $excel->getActiveSheet()->setCellValue('C'.$numrow4, $data['passweb']);
        $excel->getActiveSheet()->setCellValue('D'.$numrow, $data['ket2']);

        //apply style row yang telah dibuar tadi ke masing-masing baris
        $excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('A'.$numrow2)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('A'.$numrow3)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('A'.$numrow4)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('A'.$numrow5)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('A'.$numrow6)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('B'.$numrow2)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('B'.$numrow3)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('B'.$numrow4)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('B'.$numrow5)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('B'.$numrow6)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('C'.$numrow2)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('C'.$numrow3)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('C'.$numrow4)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('C'.$numrow5)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('C'.$numrow6)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('D'.$numrow2)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('D'.$numrow3)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('D'.$numrow4)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('D'.$numrow5)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('D'.$numrow6)->applyFromArray($style_row);

        $excel->getActiveSheet()->getRowDimension($numrow)->setRowHeight(20);
        //nambah looping
        $no+=6;
        $numrow+=6;
        $numrow2+=6;
        $numrow3+=6;
        $numrow4+=6;
        $numrow5+=6;
        $numrow6+=6;
    }

    //set width kolom
    $excel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
    $excel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
    $excel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
    $excel->getActiveSheet()->getColumnDimension('D')->setWidth(30);

    //model kertas landscape
    $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

    //set judul kertas jadi landscape
    $excel->getActiveSheet()->setTitle("Laporan Data User Komodo");
    $excel->getActiveSheet();

    //proses file excel
    header('Content-Type: application/vdn.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition:attachment; filename="Data User Komodo.xls"');

    $write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
    $write->save('php://output');

?>