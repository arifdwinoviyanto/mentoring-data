<?php
    include 'function.php';

    require_once 'PHPExcel/PHPExcel.php';

    //plugin PHPExcel
    $excel = new PHPExcel();

    //Setting awal file excel
    $excel->getProperties()->setCreator('My Notes Code')
                           ->setLastModifiedBy('My Notes Code')
                           ->setTitle('Data SMSMonitoring')
                           ->setSubject('Transaksi')
                           ->setDescription('Laporan Semua Data SMSMonitoring')
                           ->setKeywords('Data SMSMonitoring');

    //membuat variable untuk penampung pengaturan style dari header tabel
    $style_col= array(
                'font'  => array('bold' => true), //set font jadi bold
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,    
                    'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER
                        ),//set text jadi di tengah
                        'borders' => array(
                            'top'       => array('style' => PHPExcel_Style_Border::BORDER_THIN),
                            'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN),
                            'bottom'    => array('style' => PHPExcel_Style_Border::BORDER_THIN),
                            'left'      => array('style' => PHPExcel_Style_Border::BORDER_THIN)
                            )
                        );

    //membuat variable untuk menampung style dari isi table
    $style_row = array(
        'alignment' => array(
            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
        ),
        'borders' => array(
            'top'       => array('style' => PHPExcel_Style_Border::BORDER_THIN),
            'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN),
            'bottom'    => array('style' => PHPExcel_Style_Border::BORDER_THIN),
            'left'      => array('style' => PHPExcel_Style_Border::BORDER_THIN)
        )
    );

    //eksekusi
    if (isset($_GET['tgl_awal2']) && isset($_GET['tgl_akhir2'])) {
        $tgl    = date('Y-m-d', strtotime($_GET['tgl_awal2']));
        $tgl2   = date('Y-m-d', strtotime($_GET['tgl_akhir2']));

        $label = 'Data SMSMonitoring Tanggal '.$tgl.' sd '.$tgl2;

        $query = "SELECT * FROM datasmsmonitoring WHERE DATE(tanggal)>='".$_GET['tgl_awal2']."' AND DATE(tanggal)<='".$_GET['tgl_akhir2']."' ORDER BY tanggal ASC";
    } else {
        $label = 'Semua Data SMSMonitoring';

        $query = "SELECT * FROM datasmsmonitoring ORDER BY tanggal ASC";
    }

    $excel->setActiveSheetIndex(0);
    $excel->getActiveSheet()->setCellValue('A1', "Data SMSMonitoring");//Set kolom pertama
    $excel->getActiveSheet()->getStyle()->getFont()->setSize(16);
    $excel->getActiveSheet()->getStyle('A1')->applyFromArray(
        array(
            'alignment' => array(
                'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'font'       => array('size' => 16)
            )
        )
        );
    $excel->getActiveSheet()->mergeCells('A1:F1'); //set panjang margin
    $excel->getActiveSheet()->getstyle('A1')->getFont()->setBold(TRUE); //set bold kolom A1

    $excel->getActiveSheet()->setCellValue('A2', $label);
    $excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(12);
    $excel->getActiveSheet()->getStyle('A2')->applyFromArray(
        array(
            'alignment' => array(
                'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'font'       => array('size' => 12)
            )
        )
            );
    $excel->getActiveSheet()->mergeCells('A2:F2');

    //membuat header table pada baris ke 4
    $excel->getActiveSheet()->setCellValue('A4', 'Tanggal');
    $excel->getActiveSheet()->setCellValue('B4', 'Nama');
    $excel->getActiveSheet()->setCellValue('C4', 'Pagi');
    $excel->getActiveSheet()->setCellValue('D4', 'Sore');
    $excel->getActiveSheet()->setCellValue('E4', 'Jumlah/Hari');
    $excel->getActiveSheet()->setCellValue('F4', 'Keterangan');
    $excel->getActiveSheet()->getStyle('A4:F4')->getFont()->setSize(12);

    //Apply style header yang telah dibuat ke masing-masing kolom tabel
    $excel->getActiveSheet()->getStyle('A4')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('B4')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('C4')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('D4')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('E4')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('F4')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('A4:F4')->getFill()->applyFromArray(
        array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'startcolor' => array('argb' => 'FF008000')
        )
    );

    //set height bari ke 1 sampai 6
    $excel->getActiveSheet()->getRowDimension('1')->setRowHeight(20);
    $excel->getActiveSheet()->getRowDimension('2')->setRowHeight(20);
    $excel->getActiveSheet()->getRowDimension('3')->setRowHeight(20);
    $excel->getActiveSheet()->getRowDimension('4')->setRowHeight(20);
    $excel->getActiveSheet()->getRowDimension('5')->setRowHeight(20);
    $excel->getActiveSheet()->getRowDimension('6')->setRowHeight(20);
    
    $sql = mysqli_query($conn, $query); //eksekusi jalankan query dari variable
    $no = 1; //untuk penomoran pada tabel
    $numrow = 5; //baris pertama untuk diisi tabel adalah baris ke 5
    $numrow2 = 6; //baris pertama untuk diisi tabel adalah baris ke 6
    $numrow3 = 7; //baris pertama untuk diisi tabel adalah baris ke 7
    $numrow4 = 8; //baris pertama untuk diisi tabel adalah baris ke 8
    $numrow5 = 9; //baris pertama untuk diisi tabel adalah baris ke 9  

    while ($data = mysqli_fetch_array($sql)) {
        $tgl = date('d-m-Y', strtotime($data['tanggal']));
        $nama  = 'Remaining Quota';
        $nama2 = 'Total Trans';
        $nama3 = 'Queue MSG';
        $nama4 = 'Success MSG';
        $nama5 = 'Fail MSG';

        $excel->getActiveSheet()->setCellValue('A'.$numrow, $tgl);
        $excel->getActiveSheet()->setCellValue('B'.$numrow, $nama);
        $excel->getActiveSheet()->setCellValue('B'.$numrow2, $nama2);
        $excel->getActiveSheet()->setCellValue('B'.$numrow3, $nama3);
        $excel->getActiveSheet()->setCellValue('B'.$numrow4, $nama4);
        $excel->getActiveSheet()->setCellValue('B'.$numrow5, $nama5);
        $excel->getActiveSheet()->setCellValue('C'.$numrow, $data['remaining_quota_pagi']);
        $excel->getActiveSheet()->setCellValue('C'.$numrow2, $data['total_usage_pagi']);
        $excel->getActiveSheet()->setCellValue('C'.$numrow3, $data['queue_msq_pagi']);
        $excel->getActiveSheet()->setCellValue('C'.$numrow4, $data['success_trans_pagi']);
        $excel->getActiveSheet()->setCellValue('C'.$numrow5, $data['fail_trans_pagi']);
        $excel->getActiveSheet()->setCellValue('D'.$numrow, $data['remaining_quota_sore']);
        $excel->getActiveSheet()->setCellValue('D'.$numrow2, $data['total_usage_sore']);
        $excel->getActiveSheet()->setCellValue('D'.$numrow3, $data['queue_msq_sore']);
        $excel->getActiveSheet()->setCellValue('D'.$numrow4, $data['success_trans_sore']);
        $excel->getActiveSheet()->setCellValue('D'.$numrow5, $data['fail_trans_sore']);
        $excel->getActiveSheet()->setCellValue('E'.$numrow, $data['jumlah_remaining_quota']);
        $excel->getActiveSheet()->setCellValue('E'.$numrow2, $data['jumlah_total_usage']);
        $excel->getActiveSheet()->setCellValue('E'.$numrow3, $data['jumlah_queue_msq']);
        $excel->getActiveSheet()->setCellValue('E'.$numrow4, $data['jumlah_success_trans']);
        $excel->getActiveSheet()->setCellValue('E'.$numrow5, $data['jumlah_fail_trans']);
        $excel->getActiveSheet()->setCellValue('F'.$numrow, $data['keterangan']);

        // Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
        $excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('A'.$numrow2)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('A'.$numrow3)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('A'.$numrow4)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('A'.$numrow5)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('B'.$numrow2)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('B'.$numrow3)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('B'.$numrow4)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('B'.$numrow5)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('C'.$numrow2)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('C'.$numrow3)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('C'.$numrow4)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('C'.$numrow5)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('D'.$numrow2)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('D'.$numrow3)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('D'.$numrow4)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('D'.$numrow5)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('E'.$numrow)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('E'.$numrow2)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('E'.$numrow3)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('E'.$numrow4)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('E'.$numrow5)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('F'.$numrow)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('F'.$numrow2)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('F'.$numrow3)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('F'.$numrow4)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('F'.$numrow5)->applyFromArray($style_row);
        

        $excel->getActiveSheet()->getRowDimension($numrow)->setRowHeight(20);
        //menambah looping
        $no+=5;
        $numrow+=5;
        $numrow2+=5;
        $numrow3+=5;
        $numrow4+=5;
        $numrow5+=5;
    }

    //set width kolom
    $excel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
    $excel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
    $excel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
    $excel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
    $excel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
    $excel->getActiveSheet()->getColumnDimension('F')->setWidth(30);

    //model kertas landscape
    $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
    
    //Set judul kertas jadi landscape
    $excel->getActiveSheet()->setTitle("Laporan Data SMSMonitoring");
    $excel->getActiveSheet();

    header('Content-Type: application/vdn.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment; filename="Data SMSMonitoring.xls');
    header('Cache-Control: max-age=0');

    $write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
    $write->save('php://output');

?>