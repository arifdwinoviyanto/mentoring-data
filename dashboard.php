<?php
session_start(); 

if (!isset($_SESSION["user"])) {
    header('Location: login.php');
    exit;
}

require 'function.php';

$nusa       = mysqli_query($conn, "SELECT * FROM datanusa");
$monitoring = mysqli_query($conn, "SELECT * FROM datasmsmonitoring");
$komodo     = mysqli_query($conn, "SELECT * FROM datauserkomodo");

$datanusa           = mysqli_num_rows(mysqli_query($conn, "SELECT * FROM datanusa"));
$datasmsmonitoring  = mysqli_num_rows(mysqli_query($conn, "SELECT * FROM datasmsmonitoring"));
$datauserkomodo     = mysqli_num_rows(mysqli_query($conn, "SELECT * FROM datauserkomodo"))

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png" href="img/dawang.jpeg">
    <title>DLI | Monitoring</title>
    <!-- Google Font: Source Sans Pro -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
        <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
    <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bootstrap 4 -->
        <link rel="stylesheet" href="plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
    <!-- iCheck -->
        <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- JQVMap -->
        <link rel="stylesheet" href="plugins/jqvmap/jqvmap.min.css">
    <!-- Theme style -->
        <link rel="stylesheet" href="css/adminlte.min.css">
    <!-- overlayScrollbars -->
        <link rel="stylesheet" href="plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
    <!-- Daterange picker -->
        <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
    <!-- summernote -->
        <link rel="stylesheet" href="plugins/summernote/summernote-bs4.min.css">
    <!-- Script CSS dataTables -->
        <link rel="stylesheet" href='plugins/css/jquery.dataTables.min.css'>
    <!-- Script CSS responsive -->
        <link rel="stylesheet" href='plugins/css/responsive.dataTables.min.css'>
    <!-- Script CSS buttons -->
        <link rel="stylesheet" href='plugins/css/buttons.dataTables.min.css'>
</head>
<body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">
        
    <!-- navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light" id="up">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-sm-inline-block">
        <a href="dashboard.php" class="nav-link">Beranda</a>
      </li>
      <li class="nav-item d-sm-inline-block">
        <a href="keluar.php" class="nav-link">Keluar</a>
      </li>
    </ul>

    </nav>
    </div>

    <aside class="main-sidebar sidebar-dark-success elevation-4">
        <!-- Logo -->
        <div class="user-panel mt-3 pb-3 d-flex">
            <div class="image">
                <img src="img/dawang.jpeg" alt="PT Dawang Lestari Indah" class="brand-image img-circel elevation-2" style="opacity: .8">
            </div>
            <div class="info">
                <a style="color: white;">PT. Dawang Lestari Indah</a>
            </div>
        </div>
        <!-- Sidebar -->
        <div class="sidebar">
            <div class="user-panel mt-3 pb-3 d-flex">
                <div class="image">
                    <img src="img/AdminLTELogo.png" class="img-circel elevation-2" alt="Dawang Lestasi Indah">
                </div>
                <div class="info">
                    <a class="d-block" style="color: white;"><?php echo $_SESSION['user'];?></a>
                </div>
            </div>
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item menu-open">
                    <a href="#" class="nav-link active">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                            <p>Dawang Lestari Indah
                        <i class="right fas fa-angle-left"></i>
                            </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="dashboard.php" class="nav-link active">
                                <i class="fas fa-list nav-icon"></i>
                                <p>Monitoring</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="dashboard2.php" class="nav-link">
                                <i class="fas fa-folder nav-icon"></i>
                                <p>Data File</p>
                            </a>
                        </li>
                        <!-- <li class=nav-item>
                            <a href="dashboard3.php" class="nav-link">
                                <i class="fas fa-circle nav-icon"></i>
                                <p>Dashboard v3</p>
                            </a>
                        </li> -->
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
    </aside>

    <!-- content wrapper. contains page content -->
    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Monitoring</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <!-- <li class="breadcrumb-item"><a href="dashboard.php">Beranda</a></li>
                            <li class="breadcrumb-item active">Monitoring</li> -->
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-3 col-6">
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h3> <?php echo $datanusa ?> </h3>

                            <p>Data NusaSMS</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-stats-bars"></i>
                        </div>
                            <a href="#nusasms" class="small-box-footer"> Lihat Detail <i class="far fa-arrow-circel-right"></i></a>
                    </div>
                </div>
                <div class="col-lg-3 col-6">
                    <div class="small-box bg-warning">
                        <div class="inner">
                            <h3><?php echo $datasmsmonitoring ?></h3>

                            <p>Data SMSMonitoring</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-stats-bars"></i>
                        </div>
                            <a href="#smsmonitoring" class="small-box-footer">Lihat Detail <i class="far fa-arrow-circel-right"></i> </a>
                    </div>
                </div>
                <div class="col-lg-3 col-6">
                    <div class="small-box bg-success">
                        <div class="inner">
                            <h3><?php echo $datauserkomodo ?></h3>

                            <p>Data User Komodo</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-stats-bars"></i>
                        </div>
                            <a href="#userkomodo" class="small-box-footer">Lihat Detail <i class="far fa-arrow-circel-right"></i> </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header border-transparent">
                        <h3 class="card-title" id="nusasms">Data NusaSMS</h3>
                            <div class="card-tools">
                                <label>
                                    <a href="data/tambahdata/tambahnusa.php" class="btn btn-outline-primary">+Tambah Data</a>                            
                                </label>
                                <!-- <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button> -->
                            </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-12">
                            <div class="text-center">
                                <div class="small-box bg-white">
                                    <form action="<?php echo $_SERVER["PHP_SELF"] ?>" method="post" enctype="multipart/form-data">
                                        <div class="form-group">
                                            <label>Tanggal</label>
                                            <label><input type="date" class="form-control datepicker" name="tgl_awal" id="tgl_awal" value="<?php if(isset($_POST['tgl_awal'])) echo $_POST['tgl_awal']; ?>"></label>
                                            <label>sd</label>
                                            <label><input type="date" class="form-control datepicker" name="tgl_akhir" id="tgl_akhir" value="<?php if (isset($_POST['tgl_akhir'])) echo $_POST['tgl_akhir']; ?>"></label>
                                            <button type="submit" name="cari" id="cari" class="btn btn-secondary">cari</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="sms" class="table table-sm" style="100%">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tanggal</th>
                                    <th>Nama</th>
                                    <th>Pagi</th>
                                    <th>Sore</th>
                                    <th>Jumlah/Hari</th>
                                    <th>Keterangan</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                                <?php $l = 1; ?>
                            <tbody>
                                <?php 
                                    $tgl_awal = date("2021-01-01");
                                    $tgl_akhir = date("Y-m-d");

                                    if (isset($_POST['tgl_awal']) && isset($_POST['tgl_akhir'])) {
                                        $tgl_awal = date('Y-m-d', strtotime($_POST["tgl_awal"]));
                                        $tgl_akhir = date('Y-m-d', strtotime($_POST["tgl_akhir"]));
        

                                        $sql = "SELECT * FROM datanusa WHERE tanggal BETWEEN '".$tgl_awal."' AND '".$tgl_akhir."' ORDER BY tanggal DESC";
                                    } else {
                                        $sql = "SELECT * FROM datanusa ORDER BY tanggal DESC";
                                    }
                                    $data_nusa = mysqli_query($conn, $sql);
                                    while($a = mysqli_fetch_array($data_nusa)) {
                                ?>
                                <tr>
                                    <td><?php echo $l++; ?></td>
                                    <td><?php echo $a['tanggal']; ?></td>

                                    <th>
                                        SMS<br> <hr>
                                        WA 
                                    </th>
                                    <td>
                                        <?php echo $a['smspagi']; ?> <br> <hr>
                                        <?php echo $a['wapagi']; ?>
                                    </td>
                                    <td>
                                        <?php echo $a['smssore']; ?> <br> <hr>
                                        <?php echo $a['wasore']; ?>
                                    </td>
                                    <td>
                                        <?php echo $a['smsjumlah']; ?> <br> <hr>
                                        <?php echo $a['wajumlah']; ?>
                                    </td>
                                    
                                    <td><?php echo $a['ket']; ?></td>
                                    <td> 
                                        <a href="data/ubahnusa.php?id=<?php echo $a['id'];?>" class="btn btn-success">Ubah</a>
                                        <a href="data/hapusdatanusa.php?id=<?php echo $a['id'];?>" class="btn btn-danger" onclick="return confirm('Apakah anda yakin akan menghapus Data NusaSMS <?php echo $a['tanggal'];?>???')";>Hapus</a>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                            <div class="text-center">
                                <a href="export.php?tgl_awal=<?php echo $tgl_awal ?>&tgl_akhir=<?php echo $tgl_akhir ?>" name="export" id="export" class="btn btn-outline-secondary">Download Excel</a><hr>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header border-transparent">
                        <h3 class="card-title" id="smsmonitoring">Data SMSMonitoring</h3>
                        <div class="card-tools">
                            <label>
                                <a href="data/tambahdata/tambahsmsmonitoring.php" class="btn btn-outline-primary">+Tambah Data</a>                            
                            </label>
                            <!-- <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button> -->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-12">
                            <div class="text-center">
                                <div class="small-box bg-white">
                                    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data">
                                        <div class="form-group">
                                            <label>Tanggal</label>
                                            <label><input type="date" name="tgl_awal2" id="tgl_awal2" class="form-control datepicker" value="<?php if (isset($_POST['tgl_awal2'])) echo $_POST['tgl_awal2']; ?>"></label>
                                            <label>sd</label>
                                            <label><input type="date" name="tgl_akhir2" id="tgl_akhir2" class="form-control datepicker" value="<?php if (isset($_POST['tgl_akhir2'])) echo $_POST['tgl_akhir2']; ?>"></label>
                                            <button type="submit" name="cari2" id="cari2" class="btn btn-secondary">cari</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="monitoring" class="table table-sm" style="100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Tanggal</th>
                                        <th>Nama</th>
                                        <th>Pagi</th>
                                        <th>Sore</th>
                                        <th>Jumlah/Hari</th>
                                        <th>Keterangan</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                    <?php $j = 1 ?>
                                <tbody>
                                <?php 
                                    $tgl_awal2 = '2021-01-01';
                                    $tgl_akhir2 = date('Y-m-d');
                                ?>
                                    <?php
                                        if (isset($_POST['tgl_awal2']) && isset($_POST['tgl_akhir2'])) {
                                            $tgl_awal2 = date('Y-m-d', strtotime($_POST["tgl_awal2"]));
                                            $tgl_akhir2 = date('Y-m-d', strtotime($_POST["tgl_akhir2"]));
    
                                            $sql = "SELECT * FROM datasmsmonitoring WHERE tanggal BETWEEN '".$tgl_awal2."' AND '".$tgl_akhir2."' ORDER BY id DESC";
                                        } else {
                                            $sql = "SELECT * FROM datasmsmonitoring ORDER BY id DESC";
                                        }
                                        
                                        $data_smsmonitoring = mysqli_query($conn,$sql);
                                        while ($b = mysqli_fetch_array($data_smsmonitoring)) {
                                        
                                    ?>
                                    <tr>
                                        <td>
                                            <?php echo $j++; ?> 
                                        </td>
                                        <td>
                                            <?php echo $b["tanggal"]; ?>
                                        </td>
                                        
                                        <td>
                                            REMAINING QUOTA <br><hr>
                                            TOTAL TRANS     <br><hr>
                                            QUEUE MSG       <br><hr>
                                            SUCCESS MSG     <br><hr>
                                            FAIL MSG
                                        </td>
                                        <td>
                                            <?php echo $b["remaining_quota_pagi"]; ?> <br><hr>
                                            <?php echo $b["total_usage_pagi"]; ?>     <br><hr>
                                            <?php echo $b["queue_msq_pagi"]; ?>       <br><hr>
                                            <?php echo $b["success_trans_pagi"]; ?>   <br><hr>
                                            <?php echo $b["fail_trans_pagi"]; ?>
                                        </td>
                                        <td>
                                            <?php echo $b["remaining_quota_sore"]; ?>   <br><hr>
                                            <?php echo $b["total_usage_sore"]; ?>       <br><hr>
                                            <?php echo $b["queue_msq_sore"]; ?>         <br><hr>
                                            <?php echo $b["success_trans_sore"]; ?>     <br><hr>
                                            <?php echo $b["fail_trans_sore"]; ?>
                                        </td>
                                        <td>
                                            <?php echo $b["jumlah_remaining_quota"]; ?> <br><hr>
                                            <?php echo $b["jumlah_total_usage"]; ?>     <br><hr>
                                            <?php echo $b["jumlah_queue_msq"]; ?>       <br><hr>
                                            <?php echo $b["jumlah_success_trans"]; ?>   <br><hr>
                                            <?php echo $b["jumlah_fail_trans"]; ?>
                                        </td>
                                        <td>
                                            <?php echo $b["keterangan"]; ?>
                                        </td>
                                        <td>
                                            <a href="data/ubahsms.php?id=<?php echo $b['id'];?>" class="btn btn-success">Ubah</a>
                                            <a href="data/hapussmsmonitoring.php?id=<?php echo $b['id'];?>" class="btn btn-danger" onclick="return confirm('Apakah anda yakin akan menghapus Data SmsMonitoring <?php echo $b['tanggal']; ?>???')";>Hapus</a>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                                <div class="text-center">
                                    <a href="exportsms.php?tgl_awal2=<?php echo $tgl_awal2 ?>&tgl_akhir2=<?php echo $tgl_akhir2 ?>" class="btn btn-outline-secondary" name="export2" id="export2">Download Excel</a>
                                </div><hr>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header border-transparent">
                        <h3 class="card-title" id="userkomodo">Data User Komodo</h3>
                        <div class="card-tools">
                            <label>
                                <a href="data/tambahdata/tambahuserkomodo.php" class="btn btn-outline-primary">+Tambah Data</a>                            
                            </label>
                            <!-- <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button> -->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-12">
                            <div class="text-center">
                                <div class="small-box bg-white">
                                    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data">
                                        <div class="form-group">
                                            <label>Tanggal</label>
                                            <label><input type="date" name="tgl_awal3" id="tgl_awal3" class="form-control datepicker"></label>
                                            <label>sd</label>
                                            <label><input type="date" name="tgl_akhir3" id="tgl_akhir3" class="form-control datepicker"></label>
                                            <button type="submit" name="cari3" id="cari3" class="btn btn-secondary">cari</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="user" class="table table-sm" style="100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Tanggal</th>
                                        <th>Downline</th>
                                        <th>Terminal</th>
                                        <th>Keterangan</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                    <?php $k = 1; ?>
                                    <?php 
                                        $tgl_awal3 = '2021-01-01';
                                        $tgl_akhir3 = date('Y-m-d');
                                    ?>
                                <tbody>
                                    <?php
                                        if (isset($_POST['tgl_awal3']) && isset($_POST['tgl_akhir3'])) {
                                            $tgl_awal3 = date('Y-m-d', strtotime($_POST["tgl_awal3"]));
                                            $tgl_akhir3 = date('Y-m-d', strtotime($_POST["tgl_akhir3"]));

                                            $sql = "SELECT * FROM datauserkomodo WHERE tanggal BETWEEN '".$tgl_awal3."' AND '".$tgl_akhir3."' ORDER BY id DESC";
                                        } else {
                                            $sql = "SELECT * FROM datauserkomodo ORDER BY id DESC";
                                        }
                                        $user = mysqli_query($conn, $sql);
                                        while ($c = mysqli_fetch_array($user)) {
                                    ?>
                                    <tr>
                                        <td>
                                            <?php echo $k++; ?>
                                        </td>
                                        <td>
                                            <?php echo $c["tanggal"]; ?>
                                        </td>
                                        <td>
                                            <?php echo $c["nama1"]; ?>      <br> <hr>
                                            <?php echo $c["nama2"]; ?>      <br> <hr>
                                            <?php echo $c["upline"]; ?>     <br> <hr>
                                            <?php echo $c["kelharga"]; ?>   <br> <hr>
                                            <?php echo $c["markup"]; ?>     <br> <hr>
                                            <?php echo $c["markup2"]; ?>    <br> <hr>
                                        </td>
                                        <td>
                                            <?php echo $c["namatem"]; ?> <br> <hr>
                                            <?php echo $c["pemilik"]; ?> <br> <hr>
                                            <?php echo $c["passtrx"]; ?> <br> <hr>
                                            <?php echo $c["passweb"]; ?> <br> <hr>
                                        </td>
                                        <td>
                                            <?php echo $c["ket2"]; ?>
                                        </td>
                                        <td>
                                            <a href="data/ubahkomodo.php?id=<?php echo $c['id'];?>" class="btn btn-success">Ubah</a>
                                            <a href="data/hapuskomodo.php?id=<?php echo $c['id']; ?>" class="btn btn-danger" onclick="return confirm('Apakah anda yakin akan menghapus Data User Komodo <?php echo $c['nama1'];?>???')";>Hapus</a>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                            <div class="text-center">
                                <a href="exportuser.php?tgl_awal3=<?php echo $tgl_awal3 ?>&tgl_akhir3=<?php echo $tgl_akhir3 ?>" class="btn btn-outline-secondary">Download Excel</a>
                            </div><hr>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
        
    </div>

    <footer class="main-footer">
        <strong>Copyright &copy; 2020 <a href="#up" style="color: green;">PT. Dawang Lestari Indah</a>.</strong>
        All rights Reserved.
        <div class="float-right d-none d-sm-inline-block">
            <b>Version</b> 0.0.1-pre
        </div>
    </footer>
    <!-- <aside class="control-sidebar control-sidebar-dark">

    </aside> -->

    <!-- jQuery -->
        <script src="plugins/jquery/jquery.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
        <script src="plugins/jquery-ui/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
        <script>
            $.widget.bridge('uibutton', $.ui.button)
        </script>
    <!-- Bootstrap 4 -->
        <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- ChartJS -->
        <script src="plugins/chart.js/Chart.min.js"></script>
    <!-- Sparkline -->
        <script src="plugins/sparklines/sparkline.js"></script>
    <!-- JQVMap -->
        <script src="plugins/jqvmap/jquery.vmap.min.js"></script>
        <script src="plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
    <!-- jQuery Knob Chart -->
        <script src="plugins/jquery-knob/jquery.knob.min.js"></script>
    <!-- daterangepicker -->
        <script src="plugins/moment/moment.min.js"></script>
        <script src="plugins/daterangepicker/daterangepicker.js"></script>
    <!-- Tempusdominus Bootstrap 4 -->
        <script src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
    <!-- Summernote -->
        <script src="plugins/summernote/summernote-bs4.min.js"></script>
    <!-- overlayScrollbars -->
        <script src="plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
    <!-- AdminLTE App -->
        <script src="js/adminlte.js"></script>
    <!-- AdminLTE for demo purposes -->
        <script src="js/demo.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        <!-- <script src="js/pages/dashboard.js"></script> -->
        <!-- <script src="js/script.js"></script> -->
    <!--Script Javascript-->
        <script src="plugins/jquery.com/jquery-1.12.4.js"></script>
    <!-- Script Javascript jquery dataTables -->
        <script src="plugins/jquery.com/jquery.dataTables.min.js"></script>
    <!-- Script Javascript jquery dataTables.responsive -->
        <script src="plugins/jquery.com/dataTables.responsive.min.js"></script>
    <!-- <script src="plugins/jquery.com/dataTables.buttons.min.js"></script> -->
        <script src="plugins/jquery.com/buttons.colVis.min.js"></script>
    <!-- script nusaSMS -->
        <script type="text/javascript">
            (function(){
                $(".datepicker").datepicker({
                    format : 'dd-mm-yyyy',
                    autoclose : true,
                    todayHighlight : false
                });
                $('#tgl_mulai').on('changeDate', function(selected){
                    var startDate = new Date(selected.date.valuof());
                        $("#tgl_akhir").datepicker('setStartDate', startDate);
                            if($("#tgl_awal").val() > $("#tgl_awal").val()){
                                $("#tgl_akhir").val($("#tgl_akhir").val());
                            }
                        });
                });
        </script>
        <script type="text/javascript">
            $(document).ready(function(){
                $('#sms').DataTable();
            });
        </script>

    <!-- script smsMonitoring -->
        <script type="text/javascript">
            (function(){
                $(".datepicker").datepicker({
                    format : "dd-mm-yyyy",
                    autoclose : true,
                    todayHighlight : false
                });
                $('#tgl_mulai2').on('changeDate', function(selected){
                    var starDate = new Date(selected.date.valueof());
                        $('#tgl_awal2').datepicker('setStartDate', startDate);
                            if($('#tgl_awal2').val() > $('#tgl_awal2').val()){
                                $('#tgl_akhir2').val($('#tgl_akhir2').val());
                            }
                        });
                })
        </script>
        <script type="text/javascript">
            $(document).ready(function(){
                $('#monitoring').DataTable();
            });
        </script>

    <!-- script userKomodo -->
        <script type="text/javascript">
            (function(){
                $('.datepicker').datepicker({
                    format : "dd-mm-yyyy",
                    autoclose : true,
                    todayHighlight : false
                });
                $('#tgl_mulai3').on('changeDate', function(selected){
                    $('#tgl_awal3').datepicker('setStartDate', startDate);
                    if($('#tgl_awal3').val() > $('#tgl_awal3').val()){
                        $('#tgl_akhir').val($('#tgl_akhir3').val());
                    }
                });
            })
        </script>
        <script type="text/javascript">
            $(document).ready(function(){
                $('#user').DataTable();
            });
        </script>
</body>
</html>