<?php 
require '../function.php';
$keyword = $_GET['keyword'];


$query = "SELECT * FROM datanusa
          WHERE 
            tanggal   like '$keyword' OR
            smspagi   like '$keyword' OR
            smssore   like '$keyword' OR
            wapagi    like '$keyword' OR
            wasore    like '$keyword' OR
            smsjumlah like '$keyword' OR
            wajumlah  like '$keyword' OR
            ket       like '$keyword'
            ;"
    $datanusq = query($query);
?>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header border-transparent">
                        <h3 class="card-title" id="nusasms">Data NusaSMS</h3>
                        <div class="card-tools">
                            <div class="form-group">
                                <form action="" method="post">
                                        <label><input type="text" size="30" name="keyword" id="keyword" class="form-control" placeholder="Masukan Keyword Pencarian" autocomplete="off"></label>
                                    <label>
                                        <a href="data/tambahdata/tambahnusa.php" class="btn btn-primary">+Tambah Data</a>                            
                                    </label>
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="card-body p-0">
                        <div class="table-responsive">
                        <table class="table table-sm">
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Tanggal</th>
                                <th scope="col">Nama</th>
                                <th scope="col">Pagi</th>
                                <th scope="col">Sore</th>
                                <th scope="col">Jumlah/Hari</th>
                                <th scope="col">Keterangan</th>
                                <th scope="col">Aksi</th>
                            </tr>
                        </thead>
                            <?php $l = 1; ?>
                        <tbody>
                            <?php
                                $batas = 5;
                                $halaman = isset($_GET['halaman'])?(int)$_GET['halaman'] : 1;
                                $halaman_awal = ($halaman > 1)?($halaman * $batas) - $batas : 0;

                                $previos = $halaman -1;
                                $next    = $halaman +1;

                                $data = mysqli_query($conn, "SELECT * FROM datanusa");
                                $jumlah_data = mysqli_num_rows($data);
                                $total_halaman = ceil($jumlah_data/$batas);

                                $data_nusa = mysqli_query($conn, "SELECT * FROM datanusa LIMIT $halaman_awal, $batas");
                                $nomor = $halaman_awal +1;
                                while ($a = mysqli_fetch_array($data_nusa)) {
                            ?>
                            <tr>
                                <td rowspan="2"><?php echo $l++; ?></td>
                                <td rowspan="2"><?php echo $a['tanggal']; ?></td>
                                <th>SMS</th>
                                <td><?php echo $a['smspagi']; ?></td>
                                <td><?php echo $a['smssore']; ?></td>
                                <td><?php echo $a['smsjumlah']; ?></td>
                                <td rowspan="2"><?php echo $a['ket']; ?></td>
                                <td>
                                    <a href="data/ubahnusa.php?id=<?php echo $a['id'];?>" class="btn btn-success">Ubah</a>
                                    <a href="data/hapusdatanusa.php?id=<?php echo $a['id'];?>" class="btn btn-danger" onclick="return confirm('Apakah anda yakin akan menghapus Data NusaSMS <?php echo $a['tanggal'];?>???')";>Hapus</a>
                                </td>
                            </tr>
                            <tr>
                                <th>WA</th>
                                <td><?php echo $a['wapagi']; ?></td>
                                <td><?php echo $a['wasore']; ?></td>
                                <td><?php echo $a['wajumlah']; ?></td>
                            </tr>  
                        <?php } ?>
                        </tbody>
                        </table>
                            <div class="card-header border-transparent">
                                <div class="card-tools">
                                    <nav class="page navigation example">
                                        <ul class="pagination">
                                            <li class="page-item">
                                                <a class="page-link" <?php if($halaman>1) { echo "href='?halaman=$previos'"; }?>>&laquo;</a>
                                            </li>
                                                <?php
                                                    for ($a=1; $a <= $total_halaman;$a++) { 
                                                ?>
                                            <li class="page-item">
                                                <a class="page-link" href="?halaman=<?php echo $a ?>"><?php echo $a; ?></a>
                                            </li>
                                                <?php
                                                    }
                                                ?>
                                            <li class="page-item">
                                                <a class="page-link" <?php if ($halaman<$total_halaman) { echo "href='?halaman=$next'";}?>>&raquo;</a>
                                            </li>
                                        </ul>    
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>