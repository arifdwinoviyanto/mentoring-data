// menyiapkan document
$(document).ready(function(){
    //ini yang bekerja jika tombol lihat data(class="view_data") di klik
    $('.view_data').click(function(){
        //membuat variable od, nilainya dari atribut id pada button
        //id="'.$row['id'].'" -> data id dari database
        var id = $(this).attr("id");

        //mulai ajax
        $.ajax({
            url: 'ajax/detailecatatan.php',
            method: 'post',
            data: {id:id},
            success:function(data){
                $('#data_datatan').html(data);
                $('#myModal').modal("show");
            }
        });
    });
});