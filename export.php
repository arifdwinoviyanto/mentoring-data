<?php
    
include 'function.php';

    //plugin PHPExcel
    require_once 'PHPExcel/PHPExcel.php';

    //panggil class PHPExcel nya
    $excel = new PHPExcel();

    //Setting awal file excel
    $excel->getProperties()->setCreator('My Notes Code')
					       ->setLastModifiedBy('My Notes Code')
					       ->setTitle("Data NusaSMS")
					       ->setSubject("Transaksi")
					       ->setDescription("Laporan Semua Data NusaSMS")
					       ->setKeywords("Data NusaSMS");

    //membuat variable untuk menampung pengaturan style dari header tabel
    $style_col = array(
        'font'      => array('bold' => true), //set font jadi bold
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER //set text jadi di tengah secara vertical (middle)            
        ),
        'borders' => array(
            'top'    => array('style' => PHPExcel_Style_Border::BORDER_THIN),
            'right'  => array('style' => PHPExcel_Style_Border::BORDER_THIN),
            'bottom' => array('style' => PHPExcel_style_Border::BORDER_THIN),
            'left'   => array('style' => PHPExcel_Style_Border::BORDER_THIN)
        )
    );

    //membuat variable untuk menampung style dari isi table
    $style_row = array(
        'alignment' => array(
            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
        ),
        'borders'    => array(
            'top'    => array('style' => PHPExcel_Style_Border::BORDER_THIN),
            'right'  => array ('style' => PHPExcel_Style_Border::BORDER_THIN),
            'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
            'left'   => array('style' => PHPExcel_Style_Border::BORDER_THIN)
        )
    );

    if (isset($_GET['tgl_awal']) && isset($_GET['tgl_akhir'])) {
        $tgl = date('Y-m-d', strtotime($_GET["tgl_awal"]));
        $tgl2 = date('Y-m-d', strtotime($_GET["tgl_akhir"]));
        
        $label = 'Data NusaSMS Tanggal '.$tgl. ' sd ' .$tgl2;
        $query = "SELECT * FROM datanusa WHERE DATE(tanggal)>='".$_GET['tgl_awal']."' AND DATE(tanggal)<='".$_GET['tgl_akhir']."' ORDER BY tanggal ASC ";
    } else {
        $label = 'Semua Data NusaSMS';

        $query = "SELECT * FROM datanusa ORDER BY tanggal asc";
    }

    $excel->setActiveSheetIndex(0);
    $excel->getActiveSheet()->setCellValue('A1', "Data NusaSMS"); //set kolom A1 "Data NusaSMS
    $excel->getActiveSheet()->getStyle()->getFont()->setSize(16);
    $excel->getActiveSheet()->getStyle('A1')->applyFromArray(
        array(
            'alignment' => array(
                'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'font'       => array('size' => 16)
            )
        )
    );
    $excel->getActiveSheet()->mergeCells('A1:F1'); //set panjang margin 
    $excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
    
    $excel->getActiveSheet()->setCellValue('A2', $label);
    $excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(12);
    $excel->getActiveSheet()->getStyle('A2')->applyFromArray(
        array(
            'alignment' => array(
                'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'font'       => array('size' => 12)
            )
        )
    );
    $excel->getActiveSheet()->mergeCells('A2:F2');

    //membuat header table pada baris ke 4
    $excel->getActiveSheet()->setCellValue('A4', "Tanggal");
    $excel->getActiveSheet()->setCellValue('B4', "Nama");
    $excel->getActiveSheet()->setCellValue('C4', "Pagi");
    $excel->getActiveSheet()->setCellValue('D4', "Sore");
    $excel->getActiveSHeet()->setCellValue('E4', "Jumlah");
    $excel->getActiveSheet()->setCellValue('F4', "Keterangan");
    $excel->getActiveSheet()->getStyle('A4:F4')->getFont()->setSize(12);

    //Apply style header yang telah di buat ke masing-masing kolom header
    $excel->getActiveSheet()->getStyle('A4')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('B4')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('C4')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('D4')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('E4')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('F4')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('A4:F4')->getFill()->applyFromArray(
        array(
            'type' => PHPExcel_Style_FIll::FILL_SOLID,
            'startcolor' => array('argb' => 'FF008000')
        )
    );

    //set height baris ke 1 sampai 4
    $excel->getActiveSheet()->getRowDimension('1')->setRowHeight(20);
    $excel->getActiveSheet()->getRowDimension('2')->setRowHeight(20);
    $excel->getActiveSheet()->getRowDimension('3')->setRowHeight(20);
    $excel->getActiveSheet()->getRowDimension('4')->setRowHeight(20);
    $excel->getActiveSheet()->getRowDimension('5')->setRowHeight(20);
    $excel->getActiveSheet()->getRowDimension('6')->setRowHeight(20);

    $sql = mysqli_query($conn, $query); //eksekusi/jalankan query dari variabel query
    $no = 1; //untuk penomoran tabel, di awal set dengan 1
    $numrow = 5; // baris pertama untuk di isi tabel adalah baris ke 5
    $numrow2 = 6; // baris pertama untuk di isi tabel adalah baris ke 6

    while ($data = mysqli_fetch_array($sql)) {
        $tgl = date('d-m-Y', strtotime($data['tanggal']));
        $nama  = 'SMS';
        $nama2 = 'WA';

        $excel->getActiveSheet()->setCellValue('A'.$numrow, $tgl);
        $excel->getActiveSheet()->setCellValue('B'.$numrow, $nama);
        $excel->getActiveSheet()->setCellValue('B'.$numrow2, $nama2);
        $excel->getActiveSheet()->setCellValue('C'.$numrow, $data['smspagi']);
        $excel->getActiveSheet()->setCellValue('C'.$numrow2, $data['wapagi']);
        $excel->getActiveSheet()->setCellValue('D'.$numrow, $data['smssore']);
        $excel->getActiveSheet()->setCellValue('D'.$numrow2, $data['wasore']);
        $excel->getActiveSheet()->setCellValue('E'.$numrow, $data['smsjumlah']);
        $excel->getActiveSheet()->setCellValue('E'.$numrow2, $data['wajumlah']);
        $excel->getActiveSheet()->setCellValue('F'.$numrow, $data['ket']);

        //Apply style row yang telah dibuat tadi ke masing-masing baris
        $excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('A'.$numrow2)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('B'.$numrow2)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('C'.$numrow2)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('D'.$numrow2)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('E'.$numrow)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('E'.$numrow2)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('F'.$numrow)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('F'.$numrow2)->applyFromArray($style_row);

        $excel->getActiveSheet()->getRowDimension($numrow)->setRowHeight(20);
        //menambah looping
        $no+=5;
        $numrow+=2;
        $numrow2+=2;
    }

    //set width kolom
    $excel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
    $excel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
    $excel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
    $excel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
    $excel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
    $excel->getActiveSheet()->getColumnDimension('F')->setWidth(30);

    //Model kertas landscape
    $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

    //Set judul kertas jadi landscape
    $excel->getActiveSheet()->setTitle("Laporan Data NusaSMS");
    $excel->getActiveSheet();

    //proses file excel
    header('Content-Type: application/vdn.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment; filename="Data Nusa.xls"');
    header('Cache-Control: max-age=0');

    $write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
    $write->save('php://output');
?>