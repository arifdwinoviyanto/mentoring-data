<?php

session_start();

    if (!isset($_SESSION["user"])) {
        header('Location: login.php');
        exit;
    }

    require 'function.php';

    $user           = mysqli_query($conn, "SELECT * FROM user");
    $datacatatan    = mysqli_query($conn, "SELECT * FROM datacatatan");
    $datapertanyaan = mysqli_query($conn, "SELECT * FROM datapertanyaan");
    
    $users          = mysqli_num_rows(mysqli_query($conn, "SELECT * FROM user"));
    $catatan        = mysqli_num_rows(mysqli_query($conn, "SELECT * FROM datacatatan"));
    $pertanyaan     = mysqli_num_rows(mysqli_query($conn, "SELECT * FROM datapertanyaan"));

    //alert catatan
    if (isset($_POST["simpan"])) {
        if (datacatatan($_POST) > 0) {
            echo "<script>
            alert('Data Berhasil di Tambahkan!');
            document.location.href='dashboard2.php';
            </script>";
        } else {
            echo "<script>
            alert('Data Gagal di Simpan!!!');
            document.location.href='dashboard2.php';
            </script>";
        }
    }
    //alert pertanyaan
    if (isset($_POST["masuk"])) {
        if (datapertanyaan($_POST) > 0) {
            echo "<script>
                    alert('Data Berhasil di Simpan!');
                    document.location.href='dashboard2.php';
                    </script>";
        } else {
            echo "<script>
                    alert('Data Gagal di Simpan!!!');
                    document.location.href='dashboard2.php';
                    </script>";
        }
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png" href="img/dawang.jpeg">
    <title>DLI | Data File</title>
    <!-- Google Font: Source Sans Pro -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
        <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
    <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bootstrap 4 -->
        <link rel="stylesheet" href="plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
    <!-- iCheck -->
        <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- JQVMap -->
        <link rel="stylesheet" href="plugins/jqvmap/jqvmap.min.css">
    <!-- Theme style -->
        <link rel="stylesheet" href="css/adminlte.min.css">
    <!-- overlayScrollbars -->
        <link rel="stylesheet" href="plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
    <!-- Daterange picker -->
        <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
    <!-- summernote -->
        <link rel="stylesheet" href="plugins/summernote/summernote-bs4.min.css">
    <!-- bootstrap -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Script CSS dataTables -->
        <link rel="stylesheet" href='plugins/css/jquery.dataTables.min.css'>
    <!-- Script CSS responsive -->
        <link rel="stylesheet" href='plugins/css/responsive.dataTables.min.css'>
    <!-- Script CSS buttons -->
        <link rel="stylesheet" href='plugins/css/buttons.dataTables.min.css'>
  <!--Script CSS-->
<!-- <link type="text/css" href='https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css' rel='stylesheet'> -->
<!-- <link type="text/css" href='https://cdn.datatables.net/responsive/2.2.1/css/responsive.dataTables.min.css' rel='stylesheet'> -->
<!-- <link type="text/css" href='https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css' rel='stylesheet'> -->

</head>
<body class="hold-transition sidebar-mini layout-fixed" id="up">
    <div class="wrapper">
        <!-- navbar -->
        <nav class="main-header navbar navbar-expand navbar-white navbar-light">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                </li>
                <li class="nav-item d-sm-inline-block">
                    <a href="dashboard.php" class="nav-link">Beranda</a>
                </li>
                <li class="nav-item d-sm-inline-block">
                    <a href="keluar.php" class="nav-link">Keluar</a>
                </li>
            </ul>
        </nav>
    </div>

    <aside class="main-sidebar sidebar-dark-success elevation-4">
        <!-- Logo -->
        <div class="user-panel mt-3 pb-3 d-flex">
            <div class="image">
                <img src="img/dawang.jpeg" alt="PT Dawang Lestari Indah" class="brand-image img-circel elevation-2" style="opacity: .8">
            </div>
            <div class="info">
                <a style="color: white;">PT. Dawang Lestari Indah</a>
            </div>
        </div>
        <!-- Sidebar -->
        <div class="sidebar">
            <div class="user-panel mt-3 pb-3 d-flex">
                <div class="image">
                    <img src="img/AdminLTELogo.png" class="img-circel elevation-2" alt="Dawang Lestasi Indah">
                </div>
                <div class="info">
                    <a class="d-block" style="color: white;"><?php echo $_SESSION['user'];?></a>
                </div>
            </div>
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item menu-open">
                    <a href="#" class="nav-link active">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                            <p>Dawang Lestari Indah
                        <i class="right fas fa-angle-left"></i>
                            </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="dashboard.php" class="nav-link">
                                <i class="fas fa-list nav-icon"></i>
                                <p>Monitoring</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="dashboard2.php" class="nav-link active">
                                <i class="fas fa-folder nav-icon"></i>
                                <p>Data File</p>
                            </a>
                        </li>
                        <!-- <li class=nav-item>
                            <a href="dashboard3.php" class="nav-link">
                                <i class="fas fa-circle nav-icon"></i>
                                <p>Dashboard v3</p>
                            </a>
                        </li> -->
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
    </aside>

    <!-- content wrapper. contains page content -->
    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Data File</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <!-- <li class="breadcrumb-item"><a href="dashboard.php">Beranda</a></li>
                            <li class="breadcrumb-item active">Data File</li> -->
                        </ol>
                    </div>
                </div>
            </div>
        </div>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box">
                       <a href="#pertanyaann" class="info-box-icon bg-danger elevation-1"><i class="fas fa-clipboard"></i></a>
                        <div class="info-box-content">
                            <span class="info-box-text">Pertanyaan</span>
                                <span class="info-box-number"><?php echo $pertanyaan ?> </span>
                        </div>
                    </div>
                </div>
            <div class="clearfix hidden-md-up"></div>
                <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box mb-3">
                      <a href="#catatann" class="info-box-icon bg-success elevation-1"><i class="fas fa-book"></i></a>
                        <div class="info-box-content">
                          <span class="small-box-footer">Catatan</span>
                          <span class="info-box-number"><?php echo $catatan ?></span>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box mb-3">
                        <a href="#users" class="info-box-icon bg-info elevation-1"><i class="fas fa-users"></i></a>
                            <div class="info-box-content">
                                <span class="info-box-text">Users</span>
                                <span class="info-box-number"><?php echo $users ?></span>
                            </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header border-transparent">
                        <h3 class="card-title" id="pertanyaann">Pertanyaan</h3>

                        <div class="card-tools">
                                <button type="submit" class="btn btn-outline-primary" data-toggle="modal" data-target="#exampleModal">+Tambah Pertanyaan</button>
                                <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Pertanyaan Baru</h5>
                                                <button type="submit" class="btn-close" data-dismiss="modal">&times;</button>
                                            </div>
                                            <div class="modal-body">
                                                <form action="" method="post" enctype="multipart/form-data">
                                                <input type="hidden" name="id" id="id" value="<?php echo $d['id'];?>">
                                                    <div class="mb-3">
                                                        <label for="nama" class="col-form-label">Nama</label>
                                                        <input type="text" name="nama" id="nama" class="form-control" maxlength="100" autofocus placeholder="Nama" required autocomplete="off">
                                                    </div>
                                                    <div class="mb-3">
                                                        <label for="pertanyaan" class="col-form-label">Pertanyaan</label>
                                                        <input type="text" name="pertanyaan" id="petanyaan" autocomplete="off" class="form-control" maxlength="100" placeholder="Pertanyaan..">
                                                    </div>
                                                    <div class="mb-3">
                                                        <label for="jawab" class="col-form-label">Jawaban</label>
                                                        <textarea name="jawab" id="jawab" cols="30" rows="10" placeholder="Jawab" maxlength="255" class="form-control" maxlength="255"></textarea>
                                                        <p>max_200_karakter</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="submit" class="btn btn-danger" data-dismiss="modal">Batal</button>
                                                        <button type="submit" name="masuk" id="masuk" class="btn btn-primary">Simpan</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <!-- <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button> -->
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="pertanyaan" class="display" size="100%">    
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Pertanyaan</th>
                                        <th>Jawaban</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <?php $j = 1; ?>
                                
                                <tbody>
                                    <?php 
                                        $data_pertanyaan = mysqli_query($conn, "SELECT * FROM datapertanyaan ORDER BY id DESC");
                                        while($d = mysqli_fetch_array($data_pertanyaan)) {
                                    ?>
                                    <tr>
                                        <td><?php echo $j++; ?></td>
                                        <td><?php echo $d['nama']; ?></td>
                                        <td><?php echo $d['pertanyaan']; ?></td>
                                        <td><?php echo $d['jawab']; ?></td>
                                        <td>
                                            <!-- Modal Button -->
                                            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal2<?php echo $d['id'];?>">Lihat Detail</button>
                                            
                                            <div id="myModal2<?php echo $d['id'];?>" class="modal fade" role="dialog">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4>Detail Pertanyaan</h4>
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <h5>Nama</h5>
                                                            <p><?php echo $d['nama'];?></p>
                                                            <hr>
                                                            <h5>Pertanyaan</h5>
                                                            <p><?php echo $d['pertanyaan'];?></p>
                                                            <hr>
                                                            <h5>Jawab</h5>
                                                            <p><?php echo $d['jawab'];?></p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="buntton" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="hapuspertanyaan.php?id=<?php echo $d['id']; ?>" onclick="return confirm('Apakah anda yakin akan menghapus <?php echo $d['pertanyaan'];?>!!!')"; name="hapus" id="hapus" class="btn btn-danger">Hapus</a>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>    
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header border-transparent">
                        <h3 class="card-title" id="catatann">Catatan</h3>

                    <div class="card-tools">
                        <button type="submit" class="btn btn-outline-primary" data-toggle="modal" data-target="#exampleModal2">+Tambah Catatan</button>
                            <div class="modal fade" id="exampleModal2" tabindex="-1" aria-labelledby="now" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Catatan Baru</h5>
                                            <button type="button" class="btn-close" data-dismiss="modal">&times;</button>
                                        </div>
                                        <div class="modal-body">
                                            <form action="" method="post" enctype="multipart/form-data">
                                            <input type="hidden" name="noid" id="noid" value="<?php echo $f['noid'];?>">
                                            
                                                <div class="form-group">
                                                    <label for="judul" class="col-form-label">Judul</label>
                                                    <input type="text" name="judul" id="judul" class="form-control" maxlength="255" placeholder="Judul" autofocus autocomplete="off" required>
                                                </div>
                                                <div class="form-group">
                                                    <label for="ket" class="col-from-label">Keterangan</label>
                                                    <textarea name="ket" id="ket" cols="20" rows="10" class="form-control" maxlength="500" placeholder="Keterangan" autocomplete="off"></textarea>
                                                    <p>max_500_karakter</p>
                                                </div>
                                                <div class="form-group">
                                                    <label for="dokumen">Dokumen</label>
                                                    <input type="file" name="dokumen" id="dokumen"  class="form-control" value="Kosong">
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="submit" class="btn btn-danger" data-dismiss="modal">Batal</button>
                                                    <button type="submit" name="simpan" id="simpan" class="btn btn-primary">Simpan</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button> -->
                    </div>          
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                        <table id="catatan" class="display" style="100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Judul</th>
                                        <th>Keterangan</th>
                                        <th>Dokumen</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                    <?php $i = 1; ?>
                                <tbody>
                                    <?php 
                                        $row = $conn->query("SELECT * FROM datacatatan ORDER BY noid DESC");
                                        while ($f = $row->fetch_array()) {
                                    ?>
                                    <tr>
                                        <td><?php echo $i++; ?></td>
                                        <td><?php echo $f['judul']; ?></td>
                                        <td><?php echo $f['ket']; ?></td>
                                        <td><?php echo $f['dokumen'];?></td>
                                        <td>
                                            <!-- Tombol untuk menampilkan modal-->
                                            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal3<?php echo $f['noid'];?>">Lihat Detail</button>
                                        
                                            <!-- Modal -->
                                            <div id="myModal3<?php echo $f['noid'];?>" class="modal fade" role="dialog">
                                                <div class="modal-dialog">
                                                    <!-- konten modal-->
                                                    <div class="modal-content">
                                                        <!-- heading modal -->
                                                        <div class="modal-header">
                                                            <h4 class="modal-title">Detail Catatan</h4>
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        </div>
                                                        <!-- body modal -->
                                                        <div class="modal-body">
                                                            <h5>Judul</h5>
                                                            <p><?php echo $f['judul'];?></p>
                                                            <hr>
                                                            <h5>Keterangan</h5>
                                                            <p><?php echo $f['ket'];?></p>
                                                            <hr>
                                                            <h5>Dokumen</h5>
                                                            <p>
                                                                <!-- <img src="dokumen/<?php echo $f['dokumen'];?>" width="50" alt="Tidak ada gambar"><br> -->
                                                                <!-- <a href="dokumen/<?php echo $f['dokumen'];?>"> <?php echo $f['dokumen'];?> </a> -->
                                                                <a href="download.php?noid=<?php echo $f['dokumen']; ?>"><?php echo $f['dokumen']?></a>
                                                            </p>
                                                        </div>
                                                        <!-- footer modal -->
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="hapuscatatan.php?noid=<?php echo $f['noid']; ?>" onclick="return confirm('Apakah anda yakin akan menghapus <?php echo $f['judul']; ?>!!!')" class="btn btn-danger">Hapus</a>
                                        </td>
                                    </tr>
                                    
                                    <?php 
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header border-transparent" id="users">
                        <h3 class="card-title">Users</h3>

                        <div class="card-tools">
                            <!-- <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button> -->
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="user" class="display" style="100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Email</th>
                                    </tr>
                                </thead>
                                <tbody>
                                        <?php $k = 1 ?>
                                        <?php
                                            $data_user = mysqli_query($conn, "SELECT * FROM user ORDER BY id DESC");
                                            while ($g = mysqli_fetch_array($data_user)) {
                                        ?>
                                    <tr>
                                        <td><?php echo $k++; ?></td>
                                        <td><?php echo $g['nama_lengkap']; ?></td>
                                        <td><?php echo $g['email']; ?></td>
                                    </tr>
                                    <?php 
                                        }
                                    ?>
                                </tbody>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
        
    </div>

    <footer class="main-footer">
        <strong>Copyright &copy; 2020 <a href="#up" style="color: green;">PT. Dawang Lestari Indah</a>.</strong>
        All rights Reserved.
        <div class="float-right d-none d-sm-inline-block">
            <b>Version</b> 0.0.1-pre
        </div>
    </footer>
    <!-- <aside class="control-sidebar control-sidebar-dark">

    </aside> -->

    <!-- jQuery -->
        <script src="plugins/jquery/jquery.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
        <script src="plugins/jquery-ui/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
        <script>
            $.widget.bridge('uibutton', $.ui.button)
        </script>
    <!-- Bootstrap 4 -->
        <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- ChartJS -->
        <script src="plugins/chart.js/Chart.min.js"></script>
    <!-- Sparkline -->
        <script src="plugins/sparklines/sparkline.js"></script>
    <!-- JQVMap -->
        <script src="plugins/jqvmap/jquery.vmap.min.js"></script>
        <script src="plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
    <!-- jQuery Knob Chart -->
        <script src="plugins/jquery-knob/jquery.knob.min.js"></script>
    <!-- daterangepicker -->
        <script src="plugins/moment/moment.min.js"></script>
        <script src="plugins/daterangepicker/daterangepicker.js"></script>
    <!-- Tempusdominus Bootstrap 4 -->
        <script src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
    <!-- Summernote -->
        <script src="plugins/summernote/summernote-bs4.min.js"></script>
    <!-- overlayScrollbars -->
        <script src="plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
    <!-- AdminLTE App -->
        <script src="js/adminlte.js"></script>
    <!-- AdminLTE for demo purposes -->
        <script src="js/demo.js"></script>
    <!-- Jquery -->
        <script src="plugins/jquery/jquery.js"></script>
    <!-- bootstrap js -->
        <script src="js/bootstrap.min.js"></script>
    <!-- bootstrap.js -->
        <script src="plugins/bootstrap/js/bootstrap.js"></script>
    <!--Script Javascript-->
        <script src="plugins/jquery.com/jquery-1.12.4.js"></script>
    <!-- Script Javascript jquery dataTables -->
        <script src="plugins/jquery.com/jquery.dataTables.min.js"></script>
    <!-- Script Javascript jquery dataTables.responsive -->
        <script src="plugins/jquery.com/dataTables.responsive.min.js"></script>
    <!-- <script src="plugins/jquery.com/dataTables.buttons.min.js"></script> -->
        <script src="plugins/jquery.com/buttons.colVis.min.js"></script>
    <!-- <script src="https://code.jquery.com/jquery-1.12.4.js"></script> -->
    <!-- <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script> -->
    <!-- <script src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js"></script> -->
    <!-- <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script> -->
    <!-- <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script> -->
    <script>
        $(document).ready(function() {
        $('#pertanyaan').DataTable({
            dom: 'Bfrtip',
            buttons: [
            'colvis'
            ]
        });
        } );
    </script>
    <script>
        $(document).ready(function() {
        $('#catatan').DataTable( {
            dom: 'Bfrtip',
            buttons: [
            'colvis'
            ]
        } );
        } );
    </script>
    <script>
        $(document).ready(function() {
            $('#user').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                'colvis'
                ]
            });
        });
    </script>
</body>
</html>
