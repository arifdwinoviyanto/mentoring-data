<?php
session_start();
require 'function.php';

if (isset($_SESSION["user"]) ) {
    header("Location: dashboard.php");
    exit;
}

if (isset($_POST["masuk"])) {
    $namalengkap  = $_POST["nama_lengkap"];
    // $email        = $_POST["email"];
    $password     = $_POST["password"];


    $result = mysqli_query($conn, "SELECT * FROM user WHERE nama_lengkap='$namalengkap'"); 
    
    if (mysqli_num_rows($result) === 1 ) {
        
        //cek password
        $row = mysqli_fetch_assoc($result);
        if (password_verify($password, $row["password"])) {
        //set session   
        // $_SESSION["user"] = true;
        $_SESSION["user"] = $namalengkap;
        header("Location: dashboard.php");
        exit;
        }
    }
    $error = true;
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png" href="img/dawang.jpeg">
    <title>DLI | Log in</title>
    <!-- Google Font: Source Sans Pro -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
        <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
    <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bootstrap 4 -->
        <link rel="stylesheet" href="plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
    <!-- iCheck -->
        <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- JQVMap -->
        <link rel="stylesheet" href="plugins/jqvmap/jqvmap.min.css">
    <!-- Theme style -->
        <link rel="stylesheet" href="css/adminlte.min.css">
    <!-- overlayScrollbars -->
        <link rel="stylesheet" href="plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
    <!-- Daterange picker -->
        <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
    <!-- summernote -->
        <link rel="stylesheet" href="plugins/summernote/summernote-bs4.min.css">
</head>
<body class="hold-transition login-page">
    <div class="login-box">
        <div class="card card-outline card-success">
            <div class="card-header text-center">
                <a href="login.php" class="h3">PT. Dawang Lestari Indah</a>
            </div>
            <div class="card-body">
                <p class="login-box-msg">Silahkan Masuk Untuk memulai Aktivitas Anda!</p>
                <?php if (isset($error) ) : ?>
                    <script>
                        alert('Username atau Password Anda Salah!');
                    </script>
                <?php endif; ?>

                <form action="" method="POST">
                    <div class="input-group mb-3">
                        <input type="nama_lengkap" name="nama_lengkap" id="nama_lengkap" class="form-control" autofocus autocomplete="off" placeholder="Username">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-user"></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <input type="password" name="password" id="password" class="form-control" placeholder="Password">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-8">
                            <!-- <div class="icheck-primary">
                                <input type="checkbox" id="remember">
                                <label for="remember">
                                Remember Me
                                </label>
                            </div> -->
                        <!-- <p class="mb-0">
                            <a href="register.php" class="text-center">Daftar</a>
                        <p> -->
                        </div>
                        <div class="col-4">
                            <button type="submit" name="masuk" id="masuk" class="btn btn-outline-success">Masuk</button>
                        </div>
                    </div>
                </form>
                <!-- <div class="social-auth-links text-center mt-2 mb-5">
                    <p>- OR -</p>
                    <a href="#" class="btn btn-primary btn-block">
                        <i class="fab fab-facebook-plus mr-2"></i>Sign in using Facebook
                    </a>
                    <a href="#" class="btn btn-danger btn-block">
                        <i class="fab fab-google-plus mr-2"></i>Sign in using Google+
                    </a>
                </div> -->
                    <!-- /.social-auth-links -->
                    <!-- <p class="mb-1">
                        <a href="forgot.php">I forgot my password</a>
                    </p> -->
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.login-box -->
    <script src="jquery/jquery.min.js"></script>
    <script src="bootstrap/bootstrap.bundle.min.js"></script>
    <script src="js/adminlte.min.js"></script>

    <!-- jQuery -->
        <script src="plugins/jquery/jquery.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
        <script src="plugins/jquery-ui/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
        <script>
            $.widget.bridge('uibutton', $.ui.button)
        </script>
    <!-- Bootstrap 4 -->
        <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- ChartJS -->
        <script src="plugins/chart.js/Chart.min.js"></script>
    <!-- Sparkline -->
        <script src="plugins/sparklines/sparkline.js"></script>
    <!-- JQVMap -->
        <script src="plugins/jqvmap/jquery.vmap.min.js"></script>
        <script src="plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
    <!-- jQuery Knob Chart -->
        <script src="plugins/jquery-knob/jquery.knob.min.js"></script>
    <!-- daterangepicker -->
        <script src="plugins/moment/moment.min.js"></script>
        <script src="plugins/daterangepicker/daterangepicker.js"></script>
    <!-- Tempusdominus Bootstrap 4 -->
        <script src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
    <!-- Summernote -->
        <script src="plugins/summernote/summernote-bs4.min.js"></script>
    <!-- overlayScrollbars -->
        <script src="plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
    <!-- AdminLTE App -->
        <script src="js/adminlte.js"></script>
    <!-- AdminLTE for demo purposes -->
        <script src="js/demo.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <!-- <script src="js/pages/dashboard.js"></script> -->


</body>
</html>